﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 2.8.2012
 * Čas: 17:59
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Resources;
using System.Reflection;
using Bludiste1;
using System.IO; 

namespace Bludiste1
{
	
	enum Smer {
		Doprava, Doleva, Nahoru, Dolu
	}
	
	enum PolozkyMenu {
			NovaHra, Instrukce, SineSlavy, Konec
		}
	
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		//ResourceManager resources = new ResourceManager("Bludiste1.MainForm", Assembly.GetExecutingAssembly());
		Bitmap G_Hrac_Vpravo = Resources.Postava_hrace_vpravo;//(Bitmap)ResourceManager.GetObject("Postava_hrace_vpravo");
		Bitmap G_Hrac_Vlevo = Resources.Postava_hrace_vlevo;//(Bitmap)ResourceManager.GetObject("Postava_hrace_vlevo");
		Bitmap G_Hrac_Nahoru = Resources.Postava_hrace_nahoru;//(Bitmap)ResourceManager.GetObject("Postava_hrace_nahoru");
		Bitmap G_Hrac_Dolu = Resources.Postava_hrace_dolu;//(Bitmap)ResourceManager.GetObject("Postava_hrace_dolu");
		
		Bitmap G_Nevykopano = Resources.Nevykopana_Zeme;//(Bitmap)ResourceManager.GetObject("Nevykopana_zeme");
		Bitmap G_Vykopano = Resources.Vykopana_Zeme;//(Bitmap)ResourceManager.GetObject("Vykopana_Zeme");
		Bitmap G_Prekazka = Resources.Prekazka;//(Bitmap)ResourceManager.GetObject("Prekazka");
		Bitmap G_Prvni_Predmet = Resources.Prvni_Predmet;//(Bitmap)ResourceManager.GetObject("Prvni_Predmet");
		Bitmap G_Druhy_Predmet = Resources.Druhy_Predmet;//(Bitmap)ResourceManager.GetObject("Druhy_Predmet");
		Bitmap G_Vychod = Resources.Vychod;
		
		Bitmap G_Granat = Resources.Granat;
		Bitmap G_Granat1 = Resources.Granat1;
		Bitmap G_Bomba = Resources.Bomba;
		Bitmap G_Bomba1 = Resources.Bomba1;
		
		Bitmap G_Hrac1 = Resources.Hrac1;
		
		//(Bitmap)ResourceManager.GetObject("Vychod");
		//------------------------------------------------------------------------------
		/*Image G_Hrac_Vpravo = Bludiste1.Properties.Resources.Postava_hrace_vpravo;
		Image G_Hrac_Vlevo = Bludiste.Properties.Resources.Postava_hrace_vlevo;
		Image G_Hrac_Nahoru = Bludiste.Properties.Resources.Postava_hrace_nahoru;
		Image G_Hrac_Dolu = Bludiste.Properties.Resources.Postava_hrace_dolu;
		
		Image G_Nevykopano = Bludiste.Properties.Resources.Nevykopana_zeme;
		Image G_Vykopano = Bludiste.Properties.Resources.Vykopana_Zeme;
		Image G_Prekazka = Bludiste.Properties.Resources.Prekazka;
		Image G_Prvni_Predmet = Bludiste.Properties.Resources.Prvni_Predmet;
		Image G_Druhy_Predmet = Bludiste.Properties.Resources.Druhy_Predmet;
		Image G_Vychod = Bludiste.Properties.Resources.Vychod;*/
		
		public const int PolickoSirka = 40;	
		public const int PolickoVyska = 40;
		
		enum StavyPolicek {
			Nevykopano, Vykopano, Prekazka, PrvniPredmet, DruhyPredmet, Vychod, Bomba1
		}
		
		enum ObrazyHry {
			Menu, Instrukce, Hra, SineSlavy, JmenoHrace
		}
		
		ObrazyHry aktualniObraz = ObrazyHry.Menu;
		
		int pocetPredmetu = 0;
	
		
		public const int MAPA_SIRKA = 27;
		public const int MAPA_VYSKA = 16;
		
		StavyPolicek[,] Mapa = new StavyPolicek[MAPA_SIRKA, MAPA_VYSKA];
		StavyPolicek[,] MapaKlonovana;
		
		PostavaHrace Hrac;
		HlavniMenu Menu;
		
		Timer casovac = new Timer();
		Timer casovacDynamitu = new Timer();
		Timer casovacKonceHry = new Timer();
		
		int pocetSekundDoKonceHry;
		bool aktivovanaPauza = false;
		
		bool odstranitObrazekHrace = false;
		
		//int aktualniIndexCasovaceDynamitu = 0;
		ArrayList aktivniDynamity = new ArrayList();
		bool [,] umisteniDynamitu = new bool[MAPA_SIRKA, MAPA_VYSKA];
		int [,] pocetSekundDynamitu = new int[MAPA_SIRKA, MAPA_VYSKA];
		
		bool aktivovatHrace1;
		
		int mnozstviBombKRozmisteni;
		int mnozstviPrekazekKRozmisteni;
		int mnozstviPredmetuKRozmisteni;
		
		int herniUroven = 1;
		
		int pocetNeuspesnychPokusu = 0;
		
		ArrayList SineSlavy = new ArrayList();
		
		public MainForm()
		{

			InitializeComponent();
			VygenerujHerniUroven();
			VygenerujMapu();
			//Hrac = new PostavaHrace(G_Hrac_Vpravo);
			Hrac = new PostavaHrace();
			Menu = new HlavniMenu();
			casovac.Interval = 250;
			casovac.Tick += TiknutiCasovace;
			casovac.Start();
			
			casovacDynamitu.Interval = 1000;
			casovacDynamitu.Tick += TiknutiCasovaceDynamitu;
			
			casovacKonceHry.Interval = 1000;
			casovacKonceHry.Tick += TiknutiCasovaceKonceHry;
			casovacKonceHry.Start();

			aktivovatHrace1 = false;
		}
		
		private void VygenerujHerniUroven() {
			switch (herniUroven) {
				case 1:
					mnozstviBombKRozmisteni = 3;
					mnozstviPrekazekKRozmisteni = 70;
					mnozstviPredmetuKRozmisteni = 15;
					pocetSekundDoKonceHry = 300;
					break;
				case 2:
					mnozstviBombKRozmisteni = 3;
					mnozstviPrekazekKRozmisteni = 75;
					mnozstviPredmetuKRozmisteni = 20;
					pocetSekundDoKonceHry = 280;
					break;
				case 3:
					mnozstviBombKRozmisteni = 4;
					mnozstviPrekazekKRozmisteni = 80;
					mnozstviPredmetuKRozmisteni = 25;
					pocetSekundDoKonceHry = 260;
					break;
				case 4:
					mnozstviBombKRozmisteni = 4;
					mnozstviPrekazekKRozmisteni = 85;
					mnozstviPredmetuKRozmisteni = 30;
					pocetSekundDoKonceHry = 240;
					break;
				case 5:
					mnozstviBombKRozmisteni = 5;
					mnozstviPrekazekKRozmisteni = 90;
					mnozstviPredmetuKRozmisteni = 40;
					pocetSekundDoKonceHry = 220;
					break;
				case 6:
					mnozstviBombKRozmisteni = 6;
					mnozstviPrekazekKRozmisteni = 95;
					mnozstviPredmetuKRozmisteni = 45;
					pocetSekundDoKonceHry = 220;
					break;
				case 7:
					mnozstviBombKRozmisteni = 10;
					mnozstviPrekazekKRozmisteni = 100;
					mnozstviPredmetuKRozmisteni = 50;
					pocetSekundDoKonceHry = 220;
					break;
				case 8:
					mnozstviBombKRozmisteni = 12;
					mnozstviPrekazekKRozmisteni = 105;
					mnozstviPredmetuKRozmisteni = 50;
					pocetSekundDoKonceHry = 210;
					break;
				case 9:
					mnozstviBombKRozmisteni = 13;
					mnozstviPrekazekKRozmisteni = 110;
					mnozstviPredmetuKRozmisteni = 50;
					pocetSekundDoKonceHry = 205;
					break;
				case 10:
					mnozstviBombKRozmisteni = 15;
					mnozstviPrekazekKRozmisteni = 115;
					mnozstviPredmetuKRozmisteni = 50;
					pocetSekundDoKonceHry = 200;
					break;
			}
		}
		
		private void TiknutiCasovace(object o, EventArgs e) {
			ZkontrolujPadajiciPrekazky();
		}
		
		private void ZkontrolujPadajiciPrekazky() {
			bool [,] pouzitaPolicka = new bool[MAPA_SIRKA,MAPA_VYSKA];
			for (int X = 0; X < MAPA_SIRKA; X++) {
				for (int Y = 0; Y < MAPA_VYSKA; Y++) {
					if (Y + 1 < MAPA_VYSKA) {
						if (Mapa[X, Y] == StavyPolicek.Prekazka && Mapa[X, Y + 1] == StavyPolicek.Vykopano && pouzitaPolicka[X, Y] == false) {
							if (Hrac.X == X && Hrac.Y == Y + 1 && odstranitObrazekHrace == false) {
								Mapa[X, Y] = StavyPolicek.Vykopano;
								Mapa[X, Y + 1] = StavyPolicek.Prekazka;
								odstranitObrazekHrace = true;
								this.Invalidate();
								KonecHry("Nebe ti spadlo na hlavu!!!", true);
								return;
							}
							pouzitaPolicka[X, Y + 1] = true;
							Mapa[X, Y] = StavyPolicek.Vykopano;
							Mapa[X, Y + 1] = StavyPolicek.Prekazka;
							this.Invalidate();
						}
					}
				}
			}
		}
		
		private void TiknutiCasovaceDynamitu(object o, EventArgs e) {
			Point aktivniDynamit;
			int indexAktualnihoAktivnihoDynamitu = 0;	
			ArrayList indexyDynamituKVybuchnuti = new ArrayList();
				foreach (object obj in aktivniDynamity) {
					if (obj is Point) {
						aktivniDynamit = (Point) obj;
						if (pocetSekundDynamitu[aktivniDynamit.X, aktivniDynamit.Y] > 0) {
							pocetSekundDynamitu[aktivniDynamit.X, aktivniDynamit.Y]--;
							this.Invalidate();
						}
						else {
							indexyDynamituKVybuchnuti.Add(indexAktualnihoAktivnihoDynamitu);
							
							Mapa[aktivniDynamit.X, aktivniDynamit.Y] = StavyPolicek.Vykopano;
							Mapa[Math.Min(aktivniDynamit.X + 1, MAPA_SIRKA - 1), Math.Min(aktivniDynamit.Y + 1, MAPA_VYSKA - 1)] = StavyPolicek.Vykopano;
							Mapa[Math.Max(aktivniDynamit.X - 1, 0), Math.Max(aktivniDynamit.Y - 1, 0)] = StavyPolicek.Vykopano;
							Mapa[Math.Min(aktivniDynamit.X + 1, MAPA_SIRKA - 1), Math.Max(aktivniDynamit.Y - 1, 0)] = StavyPolicek.Vykopano;
							Mapa[Math.Max(aktivniDynamit.X - 1, 0), Math.Min(aktivniDynamit.Y + 1, MAPA_VYSKA - 1)] = StavyPolicek.Vykopano;
							Mapa[Math.Min(aktivniDynamit.X + 1, MAPA_SIRKA - 1), aktivniDynamit.Y] = StavyPolicek.Vykopano;
							Mapa[aktivniDynamit.X, Math.Min(aktivniDynamit.Y + 1, MAPA_VYSKA - 1)] = StavyPolicek.Vykopano;
							Mapa[Math.Max(aktivniDynamit.X - 1, 0), aktivniDynamit.Y] = StavyPolicek.Vykopano;
							Mapa[aktivniDynamit.X, Math.Max(aktivniDynamit.Y - 1, 0)] = StavyPolicek.Vykopano;
							this.Invalidate();
							
							if ((aktivniDynamit.X == Hrac.X && aktivniDynamit.Y == Hrac.Y) || 
							    (aktivniDynamit.X + 1 == Hrac.X && aktivniDynamit.Y + 1 == Hrac.Y) ||
							    (aktivniDynamit.X - 1 == Hrac.X && aktivniDynamit.Y - 1 == Hrac.Y) ||
							    (aktivniDynamit.X + 1 == Hrac.X && aktivniDynamit.Y - 1 == Hrac.Y) ||
							    (aktivniDynamit.X - 1 == Hrac.X && aktivniDynamit.Y + 1 == Hrac.Y) ||
							    (aktivniDynamit.X + 1 == Hrac.X && aktivniDynamit.Y == Hrac.Y) ||
							    (aktivniDynamit.X == Hrac.X && aktivniDynamit.Y + 1 == Hrac.Y) ||
							    (aktivniDynamit.X - 1 == Hrac.X && aktivniDynamit.Y == Hrac.Y) ||
							    (aktivniDynamit.X == Hrac.X && aktivniDynamit.Y - 1 == Hrac.Y)) {
								aktivovatHrace1 = true;
								KonecHry("Si sebevrah!!", true);
							}
							
						}
					}
					indexAktualnihoAktivnihoDynamitu++;	
				}
				if (aktivniDynamity.Count > 0) {
					foreach (object indexDynamituKVybuchnuti in indexyDynamituKVybuchnuti) {
						aktivniDynamity.RemoveAt((int) indexDynamituKVybuchnuti);
					}
				}
				else {
					aktivniDynamity = new ArrayList();
					umisteniDynamitu = new bool[MAPA_SIRKA, MAPA_VYSKA];
					pocetSekundDynamitu = new int[MAPA_SIRKA, MAPA_VYSKA];
				}
		}
		
		private void VykresliDynamity(Graphics g) {
			Point aktivniDynamit;
				foreach (object o in aktivniDynamity) {
					if (o is Point) {
						aktivniDynamit = (Point) o;
						//pocetSekundDynamitu[aktivniDynamit.X, aktivniDynamit.Y];
						g.DrawImage(G_Bomba, aktivniDynamit.X * PolickoSirka, aktivniDynamit.Y * PolickoVyska);
						StringFormat drawFormat = new StringFormat();
						drawFormat.Alignment = StringAlignment.Center;
						g.DrawString(pocetSekundDynamitu[aktivniDynamit.X, aktivniDynamit.Y].ToString(), new Font("Arial", 12, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF((float)aktivniDynamit.X * (float)PolickoSirka, (float)aktivniDynamit.Y * (float)PolickoVyska + 15.0F, 40.0F, 40.0F), drawFormat);
					}
				}
			/*g.DrawImage(G_Bomba, X * PolickoSirka, Y * PolickoVyska);*/
							/*g.DrawEllipse(Pens.Silver, X * PolickoSirka, Y * PolickoVyska, 40, 40);
							g.FillEllipse(new SolidBrush(Color.FromArgb(2, 200,200,200)), X * PolickoSirka,Y * PolickoVyska, 40, 40);*/
							/*StringFormat drawFormat = new StringFormat();
							drawFormat.Alignment = StringAlignment.Center;
							g.DrawString("5", new Font("Arial", 12, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF((float)X * (float)PolickoSirka, (float)Y * (float)PolickoVyska + 15.0F, 40.0F, 40.0F), drawFormat);*/
		}
		
		private void TiknutiCasovaceKonceHry(object o, EventArgs e) {
			if (pocetSekundDoKonceHry > 0) {
				pocetSekundDoKonceHry--;
			}
			else {
				KonecHry("Vypršel ti časový limit", true);
			}
			this.Invalidate();
		}
		
		private void VygenerujMapu() {
			Random generatorCisel = new Random();
			for (int X = 0; X < MAPA_SIRKA; X++) {
				for (int Y = 0; Y < MAPA_VYSKA; Y++) {
					Mapa[X,Y] = StavyPolicek.Nevykopano;
				}
			}
			int rozmistenePrekazky = 0;
			while (rozmistenePrekazky < mnozstviPrekazekKRozmisteni) {
				int x = generatorCisel.Next(0, MAPA_SIRKA);
				int y = generatorCisel.Next(0, MAPA_VYSKA);
				if (Mapa[x,y] == StavyPolicek.Nevykopano && (x != 0 && y != 0)) {
					rozmistenePrekazky++;
					Mapa[x,y] = StavyPolicek.Prekazka;
				}
			}
			Mapa[0,0] = StavyPolicek.Vykopano;
			int rozmistenePredmety = 0;
			while (rozmistenePredmety < mnozstviPredmetuKRozmisteni) {
				int x = generatorCisel.Next(0, MAPA_SIRKA);
				int y = generatorCisel.Next(0, MAPA_VYSKA);
				if (Mapa[x,y] == StavyPolicek.Nevykopano && (x != 0 || y != 0)) {
					if (x == (MAPA_SIRKA - 1) && Mapa[x,Math.Max(0, y - 1)] == StavyPolicek.Prekazka) {
						continue;
					}
					
					rozmistenePredmety++;
					pocetPredmetu++;
					int typPredmetu = generatorCisel.Next(0,2);
					if (typPredmetu == 0) {
						Mapa[x,y] = StavyPolicek.PrvniPredmet;
					}
					else {
						Mapa[x,y] = StavyPolicek.DruhyPredmet;
					}
					
				}
			}
			
			int rozmisteneBomby = 0;
			while (rozmisteneBomby < mnozstviBombKRozmisteni) {
				int x = generatorCisel.Next(0, MAPA_SIRKA);
				int y = generatorCisel.Next(0, MAPA_VYSKA);
				if (Mapa[x,y] == StavyPolicek.Nevykopano && (x != 0 || y != 0)) {

					rozmisteneBomby++;
					Mapa[x,y] = StavyPolicek.Bomba1;

				}
			}
			
			bool hledaniProbiha = true;
			while (hledaniProbiha) {
				int x = generatorCisel.Next(1, MAPA_SIRKA - 1);
				int y = generatorCisel.Next(1, MAPA_VYSKA - 1);
				if (Mapa[x,y] == StavyPolicek.Nevykopano) {
					Mapa[x,y] = StavyPolicek.Vychod;
					hledaniProbiha = false;
				}
			}
			MapaKlonovana = (StavyPolicek[,]) Mapa.Clone();
		}
		
		private void VykresliMapu(Graphics g) {
			for (int X = 0; X < MAPA_SIRKA; X++) {
				for (int Y = 0; Y < MAPA_VYSKA; Y++) {
					switch(Mapa[X,Y]) {
						case StavyPolicek.Nevykopano:
							g.DrawImage(G_Nevykopano, X * PolickoSirka, Y * PolickoVyska);
							           
							break;
						case StavyPolicek.Vykopano:
							g.DrawImage(G_Vykopano, X * PolickoSirka, Y * PolickoVyska);
							/*g.DrawImage(G_Bomba, X * PolickoSirka, Y * PolickoVyska);*/
							/*g.DrawEllipse(Pens.Silver, X * PolickoSirka, Y * PolickoVyska, 40, 40);
							g.FillEllipse(new SolidBrush(Color.FromArgb(2, 200,200,200)), X * PolickoSirka,Y * PolickoVyska, 40, 40);*/
							/*StringFormat drawFormat = new StringFormat();
							drawFormat.Alignment = StringAlignment.Center;
							g.DrawString("5", new Font("Arial", 12, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF((float)X * (float)PolickoSirka, (float)Y * (float)PolickoVyska + 15.0F, 40.0F, 40.0F), drawFormat);*/
							break;
						case StavyPolicek.Prekazka:
							g.DrawImage(G_Prekazka, X * PolickoSirka, Y * PolickoVyska);
							break;
						case StavyPolicek.Vychod:
							g.DrawImage(G_Vychod, X * PolickoSirka, Y * PolickoVyska);
							break;
						case StavyPolicek.PrvniPredmet:
							g.DrawImage(G_Prvni_Predmet, X * PolickoSirka, Y * PolickoVyska);
							break;
						case StavyPolicek.DruhyPredmet:
							g.DrawImage(G_Druhy_Predmet, X * PolickoSirka, Y * PolickoVyska);
							break;
						case StavyPolicek.Bomba1:
							g.DrawImage(G_Bomba1, X * PolickoSirka, Y * PolickoVyska);
							break;
					}
				}
			}
		}
		
		private void VykresliHrace(Graphics g) {
			Bitmap obrazekKvykresleni;
			
			if (aktivovatHrace1 == false) {
				switch (Hrac.SmerHrace) {
					case Smer.Doprava:
						obrazekKvykresleni = G_Hrac_Vpravo;
						break;
					case Smer.Doleva:
						obrazekKvykresleni = G_Hrac_Vlevo;
						break;
					case Smer.Nahoru:
						obrazekKvykresleni = G_Hrac_Nahoru;
						break;
					case Smer.Dolu:
						obrazekKvykresleni = G_Hrac_Dolu;
						break;
					default:
						obrazekKvykresleni = G_Hrac_Vpravo;
						break;
				}
				obrazekKvykresleni.MakeTransparent(Color.White);
			}
			else {
				obrazekKvykresleni = G_Hrac1;
			}
			/*if (umisteniDynamitu.Length != 0 && umisteniDynamitu[Hrac.X, Hrac.Y]) {
				g.DrawImage(G_Bomba, Hrac.X * PolickoSirka, Hrac.Y * PolickoVyska);
				StringFormat drawFormat = new StringFormat();
				drawFormat.Alignment = StringAlignment.Center;
				g.DrawString(pocetSekundDynamitu[Hrac.X, Hrac.Y], new Font("Arial", 12, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF((float)Hrac.X * (float)PolickoSirka, (float)Hrac.X * (float)PolickoVyska + 15.0F, 40.0F, 40.0F), drawFormat);
				g.DrawImage(obrazekKvykresleni, Hrac.X * PolickoSirka, Hrac.Y * PolickoVyska);
			}*/
			/*else {*/
				g.DrawImage(obrazekKvykresleni, Hrac.X * PolickoSirka, Hrac.Y * PolickoVyska);
			/*}*/
		}
		
		private void VykresliInstrukce(Graphics g) {
			for (int X = 0; X < MAPA_SIRKA; X++) {
				for (int Y = 0; Y < MAPA_VYSKA + PolickoVyska; Y++) {
					g.DrawImage(G_Nevykopano, X * PolickoSirka, Y * PolickoVyska);
				}
			}
			
			StringFormat drawFormat = new StringFormat();
			drawFormat.Alignment = StringAlignment.Center;
			
			g.DrawString("Instrukce", new Font("Stencil", 24, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 5, 60, 100)), new RectangleF(0.0F, 50.0F, 1080.0f, 50.0F), drawFormat);
			int i = 1;
			
			g.DrawString("1) Posbírejte všechny předměty (                  ) a vejděte do východu (         ) ve stanoveném čase", new Font("Arial", 14), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, (float)i  * 40.0F + 100.0f, 1080.0f, 50.0F), drawFormat);
			g.DrawImage(G_Prvni_Predmet, 425, i * 40 + 100 - 10);
			g.DrawImage(G_Druhy_Predmet, 465, i * 40 + 100 - 10);
			g.DrawImage(G_Vychod, 715, i * 40 + 100 - 10);
			i++;
			g.DrawString("2) Pozor na kameny(           ),které na vás mohou spadnou a zabít vás!", new Font("Arial", 14), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, (float)i  * 40.0F + 100.0f, 1080.0f, 50.0F), drawFormat);
			g.DrawImage(G_Prekazka, 428, i * 40 + 100 - 10);
			i++;
			g.DrawString("3) K likvidaci kamenů můžete použít bomby(         ), dávejte si ale pozor abyste takto nezničili jakýkoliv předmět, východ, či sami sebe!", new Font("Arial", 14), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, (float)i  * 40.0F + 100.0f, 1080.0f, 50.0F), drawFormat);
			g.DrawImage(G_Bomba, 401, i * 40 + 100 - 10);
			i++;
			g.DrawString("4) Hra obsahuje 5 úrovní, vyhrajeteli pátou úroveň vyhráváte celou hru", new Font("Arial", 14), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, (float)i  * 40.0F + 120.0f, 1080.0f, 50.0F), drawFormat);
			g.DrawString("Ovládání", new Font("Stencil", 24, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 5, 60, 100)), new RectangleF(0.0F, 350.0F, 1080.0f, 50.0F), drawFormat);
			i++;
			g.DrawString("1) Vašeho krtka ovládáte pomocí šipek, kterými také můžete posunovat kameny", new Font("Arial", 14), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, (float)i  * 40.0F + 240.0f, 1080.0f, 50.0F), drawFormat);
			i++;
			g.DrawString("2) Při stisknutém Crtl a zmáčknutí pravé či levé šipky krtek prokopne dané sousední políčko, aniž by na něj vstoupil", new Font("Arial", 14), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, (float)i  * 40.0F + 240.0f, 1080.0f, 50.0F), drawFormat);
			i++;
			g.DrawString("3) Stiskem mezerníku kladete bombu, pokud nějakou máte", new Font("Arial", 14), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, (float)i  * 40.0F + 240.0f, 1080.0f, 50.0F), drawFormat);
		}
		
		private void VykresliSinSlavy(Graphics g) {
			for (int X = 0; X < MAPA_SIRKA; X++) {
				for (int Y = 0; Y < MAPA_VYSKA + PolickoVyska; Y++) {
					g.DrawImage(G_Nevykopano, X * PolickoSirka, Y * PolickoVyska);
				}
			}
			
			StringFormat drawFormat = new StringFormat();
			drawFormat.Alignment = StringAlignment.Center;
			
			g.DrawString("Síň slávy", new Font("Stencil", 20, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 5, 60, 100)), new RectangleF(0.0F, 50.0F, 1080.0f, 50.0F), drawFormat);
			g.DrawString("Jméno", new Font("Stencil", 20, FontStyle.Bold), new SolidBrush(Color.FromArgb(255, 120,90,64)), 200.0F, 150.0F);
			g.DrawString("Skóre", new Font("Stencil", 20, FontStyle.Bold), new SolidBrush(Color.FromArgb(255, 120,90,64)), 750.0F, 150.0F);
			
			FileStream fs = new FileStream("sineSlavy.txt", FileMode.Open);
			StreamReader sr = new StreamReader(fs);

			ArrayList sinSlavy1 = new ArrayList();
				while(!sr.EndOfStream) {

					string radek = sr.ReadLine();
					string[] skoreAJmena = radek.Split(',');
					sinSlavy1.Add(new SinSlavy(skoreAJmena));
				
				}
			sr.Close();
			fs.Close();
						
			for (int i = 0; i < 10;i++) {
				
				if (i < sinSlavy1.Count) {
				SinSlavy sinSlavy = (SinSlavy) sinSlavy1[i];
					g.DrawString(sinSlavy.Jmeno, new Font("Arial", 18, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), 200.0F, 200.0F + (float)i * 40.0f);
					g.DrawString(sinSlavy.Skore, new Font("Arial", 18, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), 750.0F, 200.0F + (float)i * 40.0f);
				}
				else {
					g.DrawString("– ŽÁDNÝ ZÁZNAM –", new Font("Arial", 18, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), 200.0F, 200.0F + (float)i * 40.0f);
					g.DrawString("– – –", new Font("Arial", 18, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), 750.0F, 200.0F + (float)i * 40.0f);
				}
			}

		
		}
		
		private void VykresliJmenoHrace(Graphics g) {
			for (int X = 0; X < MAPA_SIRKA; X++) {
				for (int Y = 0; Y < MAPA_VYSKA + PolickoVyska; Y++) {
					g.DrawImage(G_Nevykopano, X * PolickoSirka, Y * PolickoVyska);
				}
			}
			
			StringFormat drawFormat = new StringFormat();
			drawFormat.Alignment = StringAlignment.Center;
			
			g.DrawString("Zadejte jméno hráče", new Font("Stencil", 24, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 5, 60, 100)), new RectangleF(0.0F, 50.0F, 1080.0f, 50.0F), drawFormat);
			g.DrawString(Hrac.JmenoHrace, new Font("Arial", 18), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, 150.0F, 1080.0f, 50.0F), drawFormat);
		}
		
		private void MainFormLoad(object sender, EventArgs e)
		{

			this.ClientSize = new Size(1080,680);
			this.BackColor = Color.Black;
			this.StartPosition = FormStartPosition.Manual;
		}
		
		private void MainFormPaint(object sender, PaintEventArgs e)
		{
			switch (aktualniObraz) { 
				case ObrazyHry.Hra:
					VykresliMapu(e.Graphics);
					if (!odstranitObrazekHrace) {
						VykresliHrace(e.Graphics);
					}
					VykresliText(e.Graphics);
					VykresliDynamity(e.Graphics);
					if (aktivovanaPauza) {
						StringFormat drawFormat = new StringFormat();
						drawFormat.Alignment = StringAlignment.Center;
						drawFormat.LineAlignment = StringAlignment.Center;
						e.Graphics.DrawString("PAUZA", new Font("Arial", 50, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, 0.0F, (float)this.ClientSize.Width, (float)this.ClientSize.Height), drawFormat);
					}
					break;
				case ObrazyHry.JmenoHrace:
					VykresliJmenoHrace(e.Graphics);
					break;
				case ObrazyHry.Instrukce:
					VykresliInstrukce(e.Graphics);
					break;
				case ObrazyHry.SineSlavy:
					VykresliSinSlavy(e.Graphics);
					break;
				case ObrazyHry.Menu:
					Menu.VykresliMenu(e.Graphics);
					break;
			}
		}
		private void MainFormKeyDown(object sender, KeyEventArgs e)
		{
			switch (aktualniObraz) {
				
				case ObrazyHry.Hra:
					if (aktivovanaPauza == true && e.KeyCode != Keys.P) {
						return;
					}
					if (e.Control) {
						switch (e.KeyCode) {
							case Keys.Right:
								if (Hrac.X + 1 < MAPA_SIRKA && Mapa[Hrac.X + 1, Hrac.Y] == StavyPolicek.Nevykopano) {
									Mapa[Hrac.X + 1, Hrac.Y] = StavyPolicek.Vykopano;
								}
								break;
							case Keys.Left:
								if (Hrac.X - 1 >= 0 && Mapa[Hrac.X - 1, Hrac.Y] == StavyPolicek.Nevykopano) {
									Mapa[Hrac.X - 1, Hrac.Y] = StavyPolicek.Vykopano;
								}
								break;
						}
						this.Invalidate();
					}
					else {
						switch (e.KeyCode) {
							case Keys.Right:
								if (Hrac.X + 1 < MAPA_SIRKA && ZkontrolujPolicko(Hrac.X + 1, Hrac.Y)) {
									Hrac.X++;
								}
								else if (Hrac.X + 2 < MAPA_SIRKA && Mapa[Hrac.X + 1, Hrac.Y] == StavyPolicek.Prekazka && Mapa[Hrac.X + 2, Hrac.Y] == StavyPolicek.Vykopano) {
									Mapa[Hrac.X + 1, Hrac.Y] = StavyPolicek.Vykopano;
									Mapa[Hrac.X + 2, Hrac.Y] = StavyPolicek.Prekazka;
									Hrac.X++;
								}
								break;
							case Keys.Left:
								if (Hrac.X - 1 >= 0 && ZkontrolujPolicko(Hrac.X -1, Hrac.Y)) {
									Hrac.X--;
								}
								else if (Hrac.X - 2 >= 0 && Mapa[Hrac.X - 1, Hrac.Y] == StavyPolicek.Prekazka && Mapa[Hrac.X - 2, Hrac.Y] == StavyPolicek.Vykopano) {
									Mapa[Hrac.X - 1, Hrac.Y] = StavyPolicek.Vykopano;
									Mapa[Hrac.X - 2, Hrac.Y] = StavyPolicek.Prekazka;
									Hrac.X--;
								}
								break;
							case Keys.Up:
								if (Hrac.Y - 1 >=0 && ZkontrolujPolicko(Hrac.X, Hrac.Y - 1)) {
									Hrac.Y--;
								}
								break;
							case Keys.Down:
								if (Hrac.Y + 1 < MAPA_VYSKA && ZkontrolujPolicko(Hrac.X, Hrac.Y + 1)) {
									Hrac.Y++;
								}
								break;
							case Keys.Space:
								if (umisteniDynamitu[Hrac.X, Hrac.Y] == false && Hrac.PocetBomb != 0) {
									Hrac.PocetBomb--;
									aktivniDynamity.Add(new Point(Hrac.X, Hrac.Y));
									umisteniDynamitu[Hrac.X, Hrac.Y] = true;
									pocetSekundDynamitu[Hrac.X, Hrac.Y] = 5;
									casovacDynamitu.Start();
								}
								break;
						}
						if (Mapa[Hrac.X, Hrac.Y] == StavyPolicek.Nevykopano) {
							Mapa[Hrac.X, Hrac.Y] = StavyPolicek.Vykopano;
						}
						if (Mapa[Hrac.X, Hrac.Y] == StavyPolicek.PrvniPredmet || Mapa[Hrac.X, Hrac.Y] == StavyPolicek.DruhyPredmet) {
							Mapa[Hrac.X, Hrac.Y] = StavyPolicek.Vykopano;
							pocetPredmetu--;
						}
						if (Mapa[Hrac.X, Hrac.Y] == StavyPolicek.Bomba1) {
							Mapa[Hrac.X, Hrac.Y] = StavyPolicek.Vykopano;
							Hrac.PocetBomb++;
						}
						if (Mapa[Hrac.X, Hrac.Y] == StavyPolicek.Vychod && pocetPredmetu == 0) {
							Hrac.skoreHrace = Hrac.skoreHrace + Hrac.PocetBomb + pocetSekundDoKonceHry - pocetPredmetu;
							if (herniUroven == 10) {
								KonecHry("Dosáhl jsi úrovně MoleGuru a vyhrál jsi celou hru! Gratuluji!", false);
							}
							else if (herniUroven == 3) {
								PostupDoLevelu("Dosáhl jsi úrovně Bažanta a  postupuješ do levelu " + ++herniUroven);
							}
							else if (herniUroven == 5) {
								PostupDoLevelu("Dosáhl jsi úrovně Kapitána a  postupuješ do levelu " + ++herniUroven);
							}
							else if (herniUroven == 7) {
								PostupDoLevelu("Dosáhl jsi úrovně Maršála a  postupuješ do levelu " + ++herniUroven);
							}
							else {
								PostupDoLevelu("Gratuluji postupuješ do levelu " + ++herniUroven);
							}
							Hrac.skoreHrace = Hrac.skoreHrace + herniUroven * 100;
						}
						this.Invalidate();
					}
					if (e.KeyCode == Keys.Escape) {
						aktualniObraz = ObrazyHry.Menu;
						this.Invalidate();
					}
					else if (e.KeyCode == Keys.P) {
						if (aktivovanaPauza) {
							aktivovanaPauza = false;
							casovac.Start();
							casovacDynamitu.Start();
							casovacKonceHry.Start();
						}
						else {
							aktivovanaPauza = true;
							casovac.Stop();
							casovacDynamitu.Stop();
							casovacKonceHry.Stop();
						}
						Invalidate();
					}
					break;
				case ObrazyHry.JmenoHrace:
					if (e.KeyCode == Keys.Escape) {
						aktualniObraz = ObrazyHry.Menu;
						this.Invalidate();
					}
					else if (e.KeyCode == Keys.Enter) {
						aktualniObraz = ObrazyHry.Hra;
						this.Invalidate();
					}
					else if (e.KeyCode == Keys.Back) {
						Hrac.JmenoHrace = Hrac.JmenoHrace.Substring(0, Math.Max(0,Hrac.JmenoHrace.Length - 1));
						this.Invalidate();
					}
					else if (e.KeyCode == Keys.Space) {
						Hrac.JmenoHrace += " ";
					}					
					else {
						if (e.Shift) {
							Hrac.JmenoHrace += "";
						}
						else {
							Hrac.JmenoHrace += e.KeyData.ToString();
						}

					}
					break;
				case ObrazyHry.Instrukce:
					if (e.KeyCode == Keys.Escape) {
						aktualniObraz = ObrazyHry.Menu;
						this.Invalidate();
					}
					break;
				case ObrazyHry.SineSlavy:
					if (e.KeyCode == Keys.Escape) {
						aktualniObraz = ObrazyHry.Menu;
						this.Invalidate();
					}
					break;
				case ObrazyHry.Menu:
					switch (e.KeyCode) {
						case Keys.Enter:
							switch (Menu.AktualniPolozkaMenu) {
								case PolozkyMenu.NovaHra:
									aktualniObraz = ObrazyHry.JmenoHrace;
									herniUroven = 1;
									pocetNeuspesnychPokusu = 0;
									VygenerujHerniUroven();
									pocetPredmetu = 0;
									Mapa = new StavyPolicek[MAPA_SIRKA, MAPA_VYSKA];
									casovac.Start();
									casovacDynamitu.Start();
									casovacKonceHry.Start();
									//pocetSekundDoKonceHry = 300;
									aktivovatHrace1 = false;
									aktivovanaPauza = false;
									odstranitObrazekHrace = false;
									aktivniDynamity = new ArrayList();
									umisteniDynamitu = new bool[MAPA_SIRKA, MAPA_VYSKA];
									pocetSekundDynamitu = new int[MAPA_SIRKA, MAPA_VYSKA];
									Hrac.X = 0;
									Hrac.Y = 0;
									Hrac.skoreHrace = 0;
									Hrac.JmenoHrace = "Hráč1";
									Hrac.PocetBomb = 0;
									VygenerujMapu();
									break;
								case PolozkyMenu.Instrukce:
									aktualniObraz = ObrazyHry.Instrukce;
									break;
								case PolozkyMenu.SineSlavy:
									aktualniObraz = ObrazyHry.SineSlavy;
									break;
								case PolozkyMenu.Konec:
									Application.Exit();
									break;
							}
							this.Invalidate();
							break;
						case Keys.Escape:
							Application.Exit();
							break;
						case Keys.Up:
							Menu.AktualniIndexPolozkyMenu--;
							this.Invalidate();
							break;
						case Keys.Down:
							Menu.AktualniIndexPolozkyMenu++;
							this.Invalidate();
							break;
					}

					break;
			}
		}
		
		private void PostupDoLevelu(string zprava) {
			casovac.Stop();
			casovacDynamitu.Stop();
			casovacKonceHry.Stop();
			MessageBox.Show(zprava, "Hra Bludiště");
			//
			aktualniObraz = ObrazyHry.Hra;
			pocetNeuspesnychPokusu = 0;			
			VygenerujHerniUroven();
			pocetPredmetu = 0;
			Mapa = new StavyPolicek[MAPA_SIRKA, MAPA_VYSKA];
			casovac.Start();
			casovacDynamitu.Start();
			casovacKonceHry.Start();
			aktivovatHrace1 = false;
			aktivovanaPauza = false;
			odstranitObrazekHrace = false;
			aktivniDynamity = new ArrayList();
			umisteniDynamitu = new bool[MAPA_SIRKA, MAPA_VYSKA];
			pocetSekundDynamitu = new int[MAPA_SIRKA, MAPA_VYSKA];
			Hrac.X = 0;
			Hrac.Y = 0;
			VygenerujMapu();			
			//
			this.Invalidate();
		}
		
		private void KonecHry(string zprava, bool opakovatKolo) {
			if (opakovatKolo && pocetNeuspesnychPokusu < 1) {
				pocetNeuspesnychPokusu++;
				casovac.Stop();
				casovacDynamitu.Stop();
				casovacKonceHry.Stop();
				MessageBox.Show(zprava, "Hra Bludiště");
				MessageBox.Show("Máš ještě jeden pokus na zvládnutí herní úrovně " + herniUroven, "Hra Bludiště");
				aktualniObraz = ObrazyHry.Hra;		
				VygenerujHerniUroven();
				pocetPredmetu = mnozstviPredmetuKRozmisteni;
				//Mapa = new StavyPolicek[MAPA_SIRKA, MAPA_VYSKA];
				casovac.Start();
				casovacDynamitu.Start();
				casovacKonceHry.Start();
				aktivovatHrace1 = false;
				aktivovanaPauza = false;
				odstranitObrazekHrace = false;
				aktivniDynamity = new ArrayList();
				umisteniDynamitu = new bool[MAPA_SIRKA, MAPA_VYSKA];
				pocetSekundDynamitu = new int[MAPA_SIRKA, MAPA_VYSKA];
				Hrac.X = 0;
				Hrac.Y = 0;
				Hrac.skoreHrace = Hrac.skoreHrace - 50 + herniUroven * 5;
				Hrac.PocetBomb = 0;
				Mapa = MapaKlonovana;
				//VygenerujMapu();			
				this.Invalidate();
			}
			else {
				casovac.Stop();
				casovacDynamitu.Stop();
				casovacKonceHry.Stop();
				ZapisSkoreHrace();
				MessageBox.Show(zprava + " Konec hry! Tvé skóre: " + Hrac.skoreHrace, "Hra Bludiště");
				aktualniObraz = ObrazyHry.Menu;
				Hrac.skoreHrace = 0;
				this.Invalidate();
			}
		}
		private void ZapisSkoreHrace() {
			if (SineSlavy.Count > 0) {
				SineSlavy.RemoveRange(0, SineSlavy.Count);
			}
			if (File.Exists("sineSlavy.txt")) {
				FileStream fs = new FileStream("sineSlavy.txt", FileMode.Open);
				StreamReader sr = new StreamReader(fs);

				while(!sr.EndOfStream) {
					string radek = sr.ReadLine();
					object radek1 = (object) radek;
					SineSlavy.Add(radek1);
					
				}
				sr.Close();
				fs.Close();
			}
			string retec = Hrac.skoreHrace + "," + Hrac.JmenoHrace;
			SineSlavy.Add((object)retec);
			
			string [] pomocnePole = (string[]) SineSlavy.ToArray(typeof(string));
			int [] pomocnePole1 = new int[pomocnePole.Length];
			for (int i = 0; i < pomocnePole.Length; i++) {
				string pomocnyRetezec = pomocnePole[i];
				string[] pomocnePole2 = pomocnyRetezec.Split(',');
				pomocnePole1[i] = Int32.Parse(pomocnePole2[0]);
				//MessageBox.Show(pomocnePole1[i].ToString(), "Hra Bludiště");
			}
	
			for (int i = 0; i < pomocnePole1.Length - 1; i++) {
				for (int j = 0; j < pomocnePole1.Length - 1; j++) {
					if (pomocnePole1[j + 1] > pomocnePole1[j]) {
							//MessageBox.Show("ZZZZZZZZZZ", "Hra Bludiště");
							int temp = pomocnePole1[j];
							string temp1 = pomocnePole[j];
							
							pomocnePole1[j] = pomocnePole1[j + 1];
							pomocnePole[j] = pomocnePole[j + 1];
							
							pomocnePole1[j + 1] = temp;
							pomocnePole[j + 1] = temp1;
							//MessageBox.Show(pomocnePole1[j].ToString(), "Hra Bludiště");
						}
				}
			}
			
			FileStream fs2 = new FileStream("sineSlavy.txt", FileMode.Create);
			StreamWriter sw = new StreamWriter(fs2);
			
			/*SineSlavy.Sort();
			SineSlavy.Reverse();*/
			for (int i = 0; i < pomocnePole.Length;i++) {
				if (i < 10) {
					sw.WriteLine(pomocnePole[i]);
				}
			}
			sw.Close();
		}
		
		private bool ZkontrolujPolicko(int x, int y) {
			return Mapa[x, y] != StavyPolicek.Prekazka; 
		}
		
		private void VykresliText(Graphics g) {
			Font pismo = new Font("Arial", 12);
			int poziceY = MAPA_VYSKA * PolickoVyska + 10;
			//g.DrawString("Počet kroků: " + Hrac.PocetKroku, pismo, Brushes.White, 5, poziceY);
			g.DrawString("Herní Úroveň: " + herniUroven.ToString(), pismo, Brushes.White, 10, poziceY);
			//g.DrawString("Úroveň pro HONZU", new Font("Arial", 20), Brushes.White, 5, poziceY);
			g.DrawString("Počet bomb: " + Hrac.PocetBomb, pismo, Brushes.White, 150, poziceY);
			g.DrawString("Počet sekund do konce hry: " + pocetSekundDoKonceHry.ToString(), pismo, Brushes.White, 280, poziceY);
			g.DrawString("Zbývá sebrat předmětů: " + pocetPredmetu, pismo, Brushes.White, 540, poziceY);
			g.DrawString("Průběžné skóre: " + Hrac.skoreHrace, pismo, Brushes.White, 760, poziceY);
			g.DrawString("|", pismo, Brushes.LightSlateGray, 135, poziceY);
			g.DrawString("|", pismo, Brushes.LightSlateGray, 265, poziceY);
			g.DrawString("|", pismo, Brushes.LightSlateGray, 525, poziceY);
			g.DrawString("|", pismo, Brushes.LightSlateGray, 745, poziceY);
		}
	}
	
	class PostavaHrace {
		/*public Bitmap Grafika {
			get;
			private set;
		}*/
		
		public string JmenoHrace = "Hráč1";
		private int SkoreHrace = 0;	
		
		public int skoreHrace {
			get {
				return SkoreHrace;
			}
			set {
				SkoreHrace = Math.Max(0, value);
			}
		}
				
		private int pocetBomb = 0;
		public int PocetBomb
		{
			get {
				return pocetBomb;
			}
			set {
				pocetBomb = value;
			}
		}
		private int pocetKroku;
		public int PocetKroku {
			get {
				return pocetKroku;
			}
			set {
				pocetKroku = value;
			}
		}
		
		private int x = 0, y = 0;
		public int X {
			get {
				return x;
			}
			set {
				if (X > value) {
					SmerHrace = Smer.Doleva;
				}
				else {
					SmerHrace = Smer.Doprava;
				}
				
				PocetKroku++;
				x = value;
			}
		}
		public int Y {
			get {
				return y;
			}
			set {
				if (Y > value) {
					SmerHrace = Smer.Nahoru;
				}
				else {
					SmerHrace = Smer.Dolu;
				}
				PocetKroku++;
				y = value;
			}
		}
		public Smer SmerHrace {
			get;
			private set;
		}
		
		public PostavaHrace() {
			/*Grafika = obrazek;
			Grafika.MakeTransparent(Color.White);*/
			SmerHrace = Smer.Doprava;
		}
	}
	
	class HlavniMenu {
		
		Bitmap G_MoleIntro = Resources.MoleIntro;
		Bitmap G_Nevykopano = Resources.Nevykopana_Zeme;
		
		PolozkyMenu aktualniPolozkaMenu;
		
		public PolozkyMenu AktualniPolozkaMenu {
			get {
				return aktualniPolozkaMenu;
			}
		}
		
		string[] polozkyMenu = new string[4];
		int aktualniIndexPolozkyMenu;
		public int AktualniIndexPolozkyMenu {
			get {
				return aktualniIndexPolozkyMenu;
			}
			set {
				if (value >= 0 && value < polozkyMenu.Length) {
					aktualniIndexPolozkyMenu = value;
				}
				else if (value < 0) {
					aktualniIndexPolozkyMenu = polozkyMenu.Length - 1;
				}
				else if (value > polozkyMenu.Length - 1) {
					aktualniIndexPolozkyMenu = 0;
				}
				switch (aktualniIndexPolozkyMenu) {
					case 0:
						aktualniPolozkaMenu = PolozkyMenu.NovaHra;
						break;
					case 1:
						aktualniPolozkaMenu = PolozkyMenu.Instrukce;
						break;
					case 2:
						aktualniPolozkaMenu = PolozkyMenu.SineSlavy;
						break;
					case 3:
						aktualniPolozkaMenu = PolozkyMenu.Konec;
						break;
				}
			}
		}
		
		public HlavniMenu() {
			polozkyMenu[0] = "Nová hra";
			polozkyMenu[1] = "Instrukce & Ovládání";
			polozkyMenu[2] = "Síň Slávy";
			polozkyMenu[3] = "Konec";
			aktualniIndexPolozkyMenu = 0;
			aktualniPolozkaMenu = PolozkyMenu.NovaHra;
		}
		
		
		
		public void VykresliMenu(Graphics g) {
			for (int X = 0; X < MainForm.MAPA_SIRKA; X++) {
				for (int Y = 0; Y < MainForm.MAPA_VYSKA + MainForm.PolickoVyska; Y++) {
					g.DrawImage(G_Nevykopano, X * MainForm.PolickoSirka, Y * MainForm.PolickoVyska);
				}
			}
			
			StringFormat drawFormat = new StringFormat();
			drawFormat.Alignment = StringAlignment.Center;
			
			g.DrawString("Bomberman v. 1.0", new Font("Arial", 24, FontStyle.Bold), new SolidBrush(Color.FromArgb(255, 120,90,64)), new RectangleF(0.0F, 50.0F, 1080.0f, 50.0F), drawFormat);
			g.DrawImage(G_MoleIntro, 140, 10);
			
			for (int i = 0; i < polozkyMenu.Length; i++) {

				if (i == aktualniIndexPolozkyMenu) {
					g.DrawString(polozkyMenu[i], new Font("Stencil", 20, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 5,60,100)), new RectangleF(0.0F, (float)i  * 50.0F + 200.0f, 1080.0f, 50.0F), drawFormat);
				}
				else {
					g.DrawString(polozkyMenu[i], new Font("Stencil", 20, FontStyle.Bold), new SolidBrush(Color.FromArgb(200, 255,255,255)), new RectangleF(0.0F, (float)i  * 50.0F + 200.0f, 1080.0f, 50.0F), drawFormat);
				}
			}
		}
	}
	
	class SinSlavy {
		public string Jmeno;
		public string Skore;
		
		public SinSlavy(string[] skoreAJmeno) {
			Jmeno = skoreAJmeno[1];
			Skore = skoreAJmeno[0];
		}
	}
}
