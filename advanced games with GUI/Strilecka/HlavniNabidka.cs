﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 13.8.2012
 * Čas: 12:42
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Strilecka
{
	
	public enum PolozkyNabidky {
		NovaHra, Konec
	}
	/// <summary>
	/// Description of HlavniNabidka.
	/// </summary>
	public partial class HlavniNabidka : UserControl
	{
		PolozkyNabidky ZvolenaPolozka;
		
		const int SirkaPolozky = 400;
		const int VyskaPolozky = 100;
		
		Rectangle ObdelnikNovaHra;
		Rectangle ObdelnikKonecHry;
		
		public delegate void DelegatVyberPolozky(PolozkyNabidky p);
		public event DelegatVyberPolozky PolozkaVybrana;
		
		public HlavniNabidka()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			this.BackColor = Color.Black;
			this.DoubleBuffered = true;
			this.Paint += ObsluhaUdalostiPaint;
			this.MouseDoubleClick += this.HlavniNabidkaMouseClick;
			
		}
		
		void HlavniNabidkaLoad(object sender, EventArgs e)
		{
			ObdelnikNovaHra = new Rectangle((this.Parent.ClientRectangle.Width - SirkaPolozky) / 2, (this.Parent.ClientRectangle.Height - VyskaPolozky) / 2 - VyskaPolozky, SirkaPolozky, VyskaPolozky);
			ObdelnikKonecHry = new Rectangle((this.Parent.ClientRectangle.Width - SirkaPolozky) / 2, (this.Parent.ClientRectangle.Height - VyskaPolozky) / 2 + VyskaPolozky, SirkaPolozky, VyskaPolozky);
			
		}
		
		private void ObsluhaUdalostiPaint(object sender, PaintEventArgs e) {
			Bitmap obrazekNovaHra = Resources.NovaHraNeoznaceno;
			Bitmap obrazekKonecHry = Resources.KonecNeoznaceno;
			
			switch (ZvolenaPolozka) {
				case PolozkyNabidky.NovaHra:
					obrazekNovaHra = Resources.NovaHraOznaceno;
					break;
				case PolozkyNabidky.Konec:
					obrazekKonecHry = Resources.KonecOznaceno;
					break;
			}
			e.Graphics.DrawImage(obrazekNovaHra, ObdelnikNovaHra);
			e.Graphics.DrawImage(obrazekKonecHry, ObdelnikKonecHry);
		}
		
		/*void HlavniNabidkaDoubleClick(object sender, EventArgs e)
		{
			if (ObdelnikNovaHra.Contains(e.L))
		}*/
		
		void HlavniNabidkaMouseClick(object sender, MouseEventArgs e)
		{
			int pocetKlepnuti = e.Clicks;
			if (ObdelnikNovaHra.Contains(e.Location)) {
				ZvolenaPolozka = PolozkyNabidky.NovaHra;
			}
			else if (ObdelnikKonecHry.Contains(e.Location)) {
				ZvolenaPolozka = PolozkyNabidky.Konec;
			}
			if (pocetKlepnuti == 2) {
				PolozkaVybrana(ZvolenaPolozka);
			}
			Invalidate();
		}
	}
}
