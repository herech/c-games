﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 13.8.2012
 * Čas: 12:29
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Strilecka
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class HlavniOkno : Form
	{
		HlavniNabidka Nabidka;
		List<Bitmap> polePozadi = new List<Bitmap>();
		
		
		public HlavniOkno()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
						
			
			this.ClientSize = new Size(1180,680);
			Nabidka = new HlavniNabidka();
			Nabidka.Parent = this;
			Nabidka.Dock = DockStyle.Fill;
			Nabidka.PolozkaVybrana += Nabidka_PolozkaVybrana;
			Nabidka.Show();
		}
		
		private void Nabidka_PolozkaVybrana(PolozkyNabidky p) {
			switch (p) {
				case PolozkyNabidky.NovaHra:
					Nabidka.Dispose();
					Nabidka = null;
					PomocnaStruktura.HerniLevel = 1;
					PomocnaStruktura.PocetNepratelKSestreleni = 15;
					PomocnaStruktura.PocetZivotu = 3;
					PomocnaStruktura.MinRychlostHorizontalni = 3;
					PomocnaStruktura.MaxRychlostHorizontalni = 6;
					PomocnaStruktura.IntervalGenerovaniNepratel = 2000;
					NastartujHru();
					break;
				case PolozkyNabidky.Konec:
					Application.Exit();
					break;
			}
		}
		public void NastartujHru() {
			/*Bitmap aktualniPozadi = Resources.pozadi1;
			switch(PomocnaStruktura.HerniLevel) {
				case 1:
					aktualniPozadi = Resources.pozadi1;
					break;
				case 2:
					aktualniPozadi = Resources.pozadi2;
					break;
				case 3:
					aktualniPozadi = Resources.pozadi3;
					break;
				case 4:
					aktualniPozadi = Resources.pozadi4;
					break;
				case 5:
					aktualniPozadi = Resources.pozadi5;
					break;
				case 6:
					aktualniPozadi = Resources.pozadi6;
					break;
				case 7:
					aktualniPozadi = Resources.pozadi7;
					break;
				case 8:
					aktualniPozadi = Resources.pozadi8;
					break;
				case 9:
					aktualniPozadi = Resources.pozadi9;
					break;
				case 10:
					aktualniPozadi = Resources.pozadi10;
					break;
				case 11:
					aktualniPozadi = Resources.pozadi11;
					break;
				case 12:
					aktualniPozadi = Resources.pozadi12;
					break;
			}*/
			this.BackColor = Color.Black;
			//this.BackgroundImage = aktualniPozadi;
			HerniSvet hra = new HerniSvet(this, this);
		}
		
		public void ObnovHlavniNabidku() {
			Nabidka = new HlavniNabidka();
			Nabidka.Parent = this;
			Nabidka.Dock = DockStyle.Fill;
			Nabidka.PolozkaVybrana += Nabidka_PolozkaVybrana;
			Nabidka.Show();
		}
		
	/*	void HlavniOknoPaint(object sender, PaintEventArgs e)
		{
		}*/
	}
	
	public abstract class HerniObjekt {
		protected Bitmap Obrazek;
		public const float Max_X = 980.0f;
		public const float Min_X = 0.0f;
		public const float Max_Y = 680.0f;
		
		public bool striliHrac;
		
		public float X {
			get;
			set;
		}
		public float Y {
			get;
			set;
		}
		
		protected HerniSvet InstanceHernihoSveta;
		
		public HerniObjekt(HerniSvet herniSvet) {
			this.InstanceHernihoSveta = herniSvet;
		}
		
		public virtual void AktualizujPohyb() {}
		
		public virtual void Vykresli(Graphics grafika) {
			grafika.DrawImage(Obrazek, X, Y);
		}
		
		public virtual void VstupHrace(object sender, KeyEventArgs e) {}
		public virtual void VstupHrace2(object sender, KeyEventArgs e) {}
		
		public virtual RectangleF ZjistiOkrajeObjektu() {
			GraphicsUnit jednotka = GraphicsUnit.Pixel;
			RectangleF relativniOkraje = Obrazek.GetBounds(ref jednotka);
			relativniOkraje.Offset(this.X, this.Y);
			return relativniOkraje;
		}
	}
	
	class Strela : HerniObjekt {
		
		protected float Rychlost = 15.0f;
		
		public override void AktualizujPohyb() {
			if (striliHrac == false) {
				Y += Rychlost;
			}
			else {
				Y -= Rychlost;
			}
			if (Y <= 0 || Y > Max_Y) {
				InstanceHernihoSveta.OdeberObjekt(this);
			}
		}
		
		public Strela(HerniSvet herniSvet, bool striliHrac) : base (herniSvet){
			if (striliHrac) {
				Rychlost = 20.0f;
				Obrazek = Resources.Strela;
			}
			else {
				Obrazek = Resources.Strela2;
				Rychlost += (float) PomocnaStruktura.HerniLevel - 0.5f;
			}
			this.striliHrac = striliHrac;
		}
	}
	
	class Nepritel : HerniObjekt {
		
		protected float RychlostHorizontalni;
		protected float RychlostVertikalni = 0.7f;
		protected int index = 0;
		protected bool vystreleno;
		
		public override void AktualizujPohyb() {
			index++;
			X += RychlostHorizontalni;
			if ((X + (float) this.Obrazek.Width / 2 >= InfoOHraci.X - (float) InfoOHraci.SirkaHrace / 2 && X + (float) this.Obrazek.Width / 2 <= InfoOHraci.X + (float) InfoOHraci.SirkaHrace / 2 ) && vystreleno == false) {
				vystreleno = true;
				Strela strela = new Strela(this.InstanceHernihoSveta, false);
				strela.X = this.X + (float) this.Obrazek.Width / 2;
				strela.Y = this.Y + (float) this.Obrazek.Height;
				InstanceHernihoSveta.PridejObjekt(strela);
			}
			
			if (index % 2 == 0) {
				Y += RychlostVertikalni;
			}
			else {
				Y += 1.4f;
			}
			if (X > Max_X - (float) this.Obrazek.Width || X < Min_X) {
				RychlostHorizontalni *= -1;
				Y += 5.0f;
				vystreleno = false;
			}
			if (Y > Max_Y) {
				InstanceHernihoSveta.OdeberObjekt(this);
			}
		}
		
		public Nepritel(HerniSvet herniSvet, Bitmap obrazek) : base (herniSvet) {
			Obrazek = obrazek;
			Obrazek.MakeTransparent(Color.White);
			Random rand = new Random();
			RychlostHorizontalni = (float) rand.Next(PomocnaStruktura.MinRychlostHorizontalni, PomocnaStruktura.MaxRychlostHorizontalni + 1);
			RychlostHorizontalni -= 0.5f;
		}
	}
	
	class Hrac : HerniObjekt {
		Timer casovac = new Timer();
		Timer casovac2 = new Timer();
		private int index = 1;
		Keys posledniKlavesa = Keys.Enter;
		
		public Hrac(HerniSvet herniSvet) : base(herniSvet) {
			Obrazek = Resources.Hrac;
			Obrazek.MakeTransparent(Color.White);
			
			this.Y = Max_Y - (float) Obrazek.Height;
			this.X = (Max_X - (float) Obrazek.Width) / 2;
			
			InfoOHraci.SirkaHrace = Obrazek.Width;
			
			casovac.Interval = 500;
			casovac.Tick += TiknutiCasovace;
			casovac2.Interval = 10;
			casovac2.Tick += TiknutiCasovace2;
		}
		
		private void TiknutiCasovace(object sender, EventArgs e) {
			index = 1;
		}
		
		private void TiknutiCasovace2(object sender, EventArgs e) {
			switch (posledniKlavesa) {
				case Keys.Right:
					if (X + (float) Obrazek.Width < Max_X) {
						X += 15.0f;
					}
					else {
						casovac2.Stop();
					}
					break;
				case Keys.Left:
					if (X > Min_X) {
						X -= 15.0f;
					}
					else {
						casovac2.Stop();
					}
					break;
				default:
					break;
			}
		}
		
		public override void VstupHrace(object sender, KeyEventArgs e) {
			switch (e.KeyCode) {
				case Keys.Right:
					if (X + (float) Obrazek.Width < Max_X) {
						posledniKlavesa = Keys.Right;
						casovac2.Start();
						X += 15.0f;
					}
					break;
				case Keys.Left:
					if (X > Min_X) {
						posledniKlavesa = Keys.Left;
						casovac2.Start();
						X -= 15.0f;
					}
					break;
				case Keys.Space:
					if (index == 1) {
						casovac.Stop();
						index = 0;
						Strela strela = new Strela(this.InstanceHernihoSveta, true);
						strela.X = this.X + (float) this.Obrazek.Width / 2;
						strela.Y = this.Y;
						InstanceHernihoSveta.PridejObjekt(strela);
						casovac.Start();
					}
					break;
			}
			InfoOHraci.X = X + (float) this.Obrazek.Width / 2;
		}
		public override void VstupHrace2(object sender, KeyEventArgs e) {
			casovac2.Stop();
		}
	}
	
	public struct InfoOHraci {
		public static float X = 0.0f;
		public static int SirkaHrace = 0;
	}
}
