﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 13.8.2012
 * Čas: 12:29
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.Windows.Forms;

namespace Strilecka
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new HlavniOkno());
		}
		
	}
}
