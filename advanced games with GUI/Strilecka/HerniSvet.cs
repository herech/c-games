﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 13.8.2012
 * Čas: 14:12
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Strilecka
{
	/// <summary>
	/// Description of HerniSvet.
	/// </summary>
	public class HerniSvet
	{
		Timer casovac = new Timer();
		Timer GeneratorNepratel = new Timer();
		List<HerniObjekt> SeznamHernichObjektu = new List<HerniObjekt>();
		Form HlavniOkno;
		HlavniOkno hlavniOkno;
		
		int PocetNepratelKSestreleni = PomocnaStruktura.PocetNepratelKSestreleni;
		
		public void PridejObjekt(HerniObjekt obj) {
			this.SeznamHernichObjektu.Add(obj);
		}
		public void OdeberObjekt(HerniObjekt obj) {
			this.SeznamHernichObjektu.Remove(obj);
		}
		
		public HerniSvet(Form f, HlavniOkno hlavniOkno)
		{
			HlavniOkno = f;
			this.hlavniOkno = hlavniOkno;
			casovac.Interval = 10;
			casovac.Tick += AktualizaceHry;
			HlavniOkno.Paint += HlavniOknoPaint;
			HlavniOkno.KeyDown += HlavniOknoKeyDown;
			HlavniOkno.KeyUp += HlavniOknoKeyUp;
			Hrac lodHrace = new Hrac(this);
			SeznamHernichObjektu.Add(lodHrace);
			Nepritel nepritel = new Nepritel(this, Resources.Nepritel1);
			SeznamHernichObjektu.Add(nepritel);
			casovac.Start();
			
			GeneratorNepratel.Interval = PomocnaStruktura.IntervalGenerovaniNepratel;
			GeneratorNepratel.Tick += GeneratorNepratelTick;
			GeneratorNepratel.Start();
		}
		
		private void AktualizaceHry(object sender, EventArgs e) {
			for (int i = 0; i < SeznamHernichObjektu.Count; ++i) {
				SeznamHernichObjektu[i].AktualizujPohyb();
			}
			ZkontrolujKolize();
			HlavniOkno.Invalidate();
		}
		
		private void GeneratorNepratelTick(object sender, EventArgs e) {
			Random rand = new Random();
			Nepritel nepritel = null;
			if (rand.Next(2) == 0) {
				nepritel = new Nepritel(this, Resources.Nepritel1);
			}
			else {
				nepritel = new Nepritel(this, Resources.Nepritel2);
			}
			nepritel.X = (float) rand.Next((int)HerniObjekt.Max_X - 100);
			
			for (int i = 0; i < this.SeznamHernichObjektu.Count; ++i) {
				if (ObjektyKoliduji(this.SeznamHernichObjektu[i], nepritel)) {
					return;
				}
					
			}
			
			SeznamHernichObjektu.Add(nepritel);
		}
		
		private void HlavniOknoPaint(object sender, PaintEventArgs e)
		{
			foreach (HerniObjekt o in SeznamHernichObjektu) {
				o.Vykresli(e.Graphics);
			}
			e.Graphics.DrawRectangle(new Pen(Color.FromArgb(0, 100,100,100)),new Rectangle(980,0, 200, 680));
			e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(50,50,50)),new Rectangle(980,0, 200, 680));
			e.Graphics.DrawLine(new Pen(Color.FromArgb(100,100,100), 1.0f), 980, 0, 980, 680);
			e.Graphics.DrawString("Herní level: " + PomocnaStruktura.HerniLevel.ToString(),new Font("Arial", 12), Brushes.LightGray, 1000.0f, 20.0f);
			e.Graphics.DrawString("Sestřelit nepřátel: " + PocetNepratelKSestreleni.ToString(),new Font("Arial", 12), Brushes.LightGray, 1000.0f, 50.0f);
			e.Graphics.DrawString("Počet životů: " + PomocnaStruktura.PocetZivotu.ToString(),new Font("Arial", 12), Brushes.LightGray, 1000.0f, 80.0f);
			
		}
		
		private void HlavniOknoKeyDown(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Escape) {
				HraKonci("Konec Hry!");
				//return;
			}
			else {
				for (int i = 0; i < SeznamHernichObjektu.Count; ++i) {
					SeznamHernichObjektu[i].VstupHrace(sender, e);
				}
			}
		}
		
		private void HlavniOknoKeyUp(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right) {
				for (int i = 0; i < SeznamHernichObjektu.Count; ++i) {
					SeznamHernichObjektu[i].VstupHrace2(sender, e);
				}
			}
		}
		
		private void ZkontrolujKolize() {
			for (int i = 0; i < this.SeznamHernichObjektu.Count; ++i) {
				for (int j = i + 1; j < this.SeznamHernichObjektu.Count; ++j) {
					HerniObjekt prvni = this.SeznamHernichObjektu[i];
					HerniObjekt druhy = this.SeznamHernichObjektu[j];
					if ((prvni is Strela) && (druhy is Nepritel) || (prvni is Nepritel) && (druhy is Strela)) {
						if (ObjektyKoliduji(prvni, druhy) && (prvni is Strela)) {
							if ((prvni.striliHrac == true)) {
								OdeberObjekt(prvni);
								OdeberObjekt(druhy);
								PocetNepratelKSestreleni--;
								if (PocetNepratelKSestreleni == 0) {
									PomocnaStruktura.HerniLevel++;
									if (PomocnaStruktura.HerniLevel == 11) {
										HraKonci("Konec Hry!");
									}
									else {
										PostupDoLevelu("Postupuješ do levelu " + PomocnaStruktura.HerniLevel);
									}
								}
							}
						}
						else if (ObjektyKoliduji(prvni, druhy) && (druhy is Strela)) {
							if ((druhy.striliHrac == true)) {
								OdeberObjekt(prvni);
								OdeberObjekt(druhy);
								PocetNepratelKSestreleni--;
								if (PocetNepratelKSestreleni == 0) {
									PomocnaStruktura.HerniLevel++;
									if (PomocnaStruktura.HerniLevel > 12) {
										HraKonci("Vyhrál jsi celou hru. Gratuluji!");
									}
									else {
										PostupDoLevelu("Postupuješ do levelu " + PomocnaStruktura.HerniLevel);
									}
								}
							}
						}
					}
					else if ((prvni is Nepritel) && (druhy is Hrac) || (prvni is Hrac) && (druhy is Nepritel)) {
						if (ObjektyKoliduji(prvni, druhy)) {
							if (druhy is Hrac) {
								OdeberObjekt(prvni);
							}
							else if (prvni is Hrac) {
								OdeberObjekt(druhy);
							}
							PomocnaStruktura.PocetZivotu--;
							if (PomocnaStruktura.PocetZivotu == 0) {
								HraKonci("Konec Hry!");
							}
						}
					}
					else if ((prvni is Strela) && (druhy is Hrac) || (prvni is Hrac) && (druhy is Strela)) {
						if (ObjektyKoliduji(prvni, druhy)) {
							if (druhy is Hrac) {
								OdeberObjekt(prvni);
							}
							else if (prvni is Hrac) {
								OdeberObjekt(druhy);
							}
							PomocnaStruktura.PocetZivotu--;
							if (PomocnaStruktura.PocetZivotu == 0) {
								HraKonci("Konec Hry!");
							}
						}
					}
				}
			}
		}
		
		private void HraKonci(string zprava) {
			SeznamHernichObjektu = new List<HerniObjekt>();
			casovac.Stop();
			GeneratorNepratel.Stop();
			MessageBox.Show(zprava);
			hlavniOkno.ObnovHlavniNabidku();
		}
		
		private void PostupDoLevelu(string zprava) {
			SeznamHernichObjektu = new List<HerniObjekt>();
			casovac.Stop();
			GeneratorNepratel.Stop();
			MessageBox.Show(zprava);
			PomocnaStruktura.PocetNepratelKSestreleni += 5;
			if (PomocnaStruktura.HerniLevel > 5) {
				PomocnaStruktura.PocetZivotu = 5;
			}
			else if (PomocnaStruktura.HerniLevel <= 5){
				PomocnaStruktura.PocetZivotu = 3;
			}
			else if (PomocnaStruktura.HerniLevel > 10){
				PomocnaStruktura.PocetZivotu = 7;
			}
			PomocnaStruktura.MinRychlostHorizontalni += 1;
			PomocnaStruktura.MaxRychlostHorizontalni += 1;
			PomocnaStruktura.IntervalGenerovaniNepratel -= 100;
			hlavniOkno.NastartujHru();
		}
		
		private bool ObjektyKoliduji(HerniObjekt prvni, HerniObjekt druhy) {
			RectangleF obdelnikPrvni = prvni.ZjistiOkrajeObjektu();
			RectangleF obdelnikDruhy = druhy.ZjistiOkrajeObjektu();
			return obdelnikPrvni.IntersectsWith(obdelnikDruhy);
		}
	}
	
	public struct PomocnaStruktura {
		public static int HerniLevel;
		public static int PocetNepratelKSestreleni;
		public static int PocetZivotu;
		public static int MinRychlostHorizontalni;
		public static int MaxRychlostHorizontalni;
		public static int IntervalGenerovaniNepratel;
	}
}
