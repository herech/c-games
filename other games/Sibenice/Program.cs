﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 12.7.2012
 * Čas: 12:48
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;

namespace Sibenice
{
	class Program
	{
		public static void Main(string[] args)
		{
			string[] slova = new string[6];
			slova[0] = "To mám chodit v amnljavhmvišzýšžřěšžášřšv4285v48484v56468včřvčmápytli od brambor?automobil";
			slova[1] = "automobil";
			slova[2] = "raketa";
			slova[3] = "králík";
			slova[4] = "muž";
			slova[5] = "nůž";
			
			int celkoveSkore = 0;
			
			for (int x = 0;x < slova.Length; x++) {
				
				string hadaneSlovo = slova[x];
				int zbyvajiciPokusy = hadaneSlovo.Length;
				short uhodnutaPismena = 0;
				char hadanyZnak;
				char[] znakySlova = new char[hadaneSlovo.Length];
				
				for (int i = 0; i < hadaneSlovo.Length; i++) {
					znakySlova[i] = '*';
				}
				bool hraProbiha = true;
				
				while(hraProbiha) {
					Console.WriteLine(znakySlova);
					Console.WriteLine("Zadejte písmeno. Počet pokusů: " + zbyvajiciPokusy);
					string zadanyText = Console.ReadLine();
					if (zadanyText.Length == 0) {
						continue;
					}
					hadanyZnak = zadanyText[0];
					zbyvajiciPokusy--;
					
					int puvodniPocet = uhodnutaPismena;
					for (int i = 0; i < znakySlova.Length; i++) {
						if(znakySlova[i] == '*') {
							if (hadaneSlovo[i] == hadanyZnak) {
								znakySlova[i] = hadanyZnak;
								uhodnutaPismena++;
								//zbyvajiciPokusy++;
								celkoveSkore++;
							}
						}
					}
					if (puvodniPocet == uhodnutaPismena)
							celkoveSkore--;
					if ((zbyvajiciPokusy == 0) || (uhodnutaPismena == hadaneSlovo.Length)) {
						hraProbiha = false;
					}
				}
				
				if (zbyvajiciPokusy == 0) {
					Console.WriteLine("Jsi dilina! Slovo které jsi neuhodl bylo: " + hadaneSlovo);
					Console.WriteLine("Tvé průběžné skóre je: " + celkoveSkore);
					Console.ReadKey();
					break;
				}
				else {
					Console.WriteLine("Uhodl jsi celé slovo! Tvé průběžné skóre je: " + celkoveSkore);
				}
				
			}
		}
	}
}