﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 26.7.2012
 * Čas: 17:34
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.Collections;
using System.Threading;

namespace Pexeso
{
	struct Pozice {
			private readonly int x;
			private readonly int y;
			
			public int X {
				get {
					return x;
				}
			}
			
			public int Y {
				get {
					return y;
				}
			}
			
			public Pozice(int x, int y) {
				this.x = x;
				this.y = y;
			}
			
	}
		
	class HerniObrazovka {
			
			enum StavKarty {
				Skryta, Otocena, Odstranena, Mezera
			}
		
			int OdhaleneKarty = 0, PocitadloTahu = 0;		
			int Rozmer = 0;
			int RozmerPouzeKaret = 6;
			char[,] HerniPole;
			StavKarty[,] PoleStavu;
			
			char RubKarty = '#';
			Pozice OtocenaKarta;
			
			protected Hrac hrac1;
			protected Hrac hrac2;
			protected Hrac aktualniHrac;
			
			public HerniObrazovka() {
				hrac1 = Program.hraci[0];
				hrac2 = Program.hraci[1];
				
				Rozmer = RozmerPouzeKaret * 2 - 1;
				HerniPole = new char[Rozmer + Rozmer - 1,Rozmer + Rozmer - 1];
				PoleStavu = new StavKarty[Rozmer + Rozmer - 1, Rozmer + Rozmer - 1];
				RozdejKarty();
				VykresliPlochu();
				Console.ReadKey();
			}
			
			private void RozdejKarty() {
				ArrayList volnePozice = new ArrayList();
				ArrayList mezeryPozice = new ArrayList();
				for (int y = 0; y < RozmerPouzeKaret; y++) {
					for (int x = 0; x < Rozmer; x++) {
						if (x == 0 || (x % 2 == 0)) {
							PoleStavu[x,y] = StavKarty.Skryta;
							Pozice pozice = new Pozice(x, y);
							volnePozice.Add(pozice);
						}
						else {
							PoleStavu[x,y] = StavKarty.Mezera;
							Pozice pozice = new Pozice(x, y);
							mezeryPozice.Add(pozice);
						}
					}
				}
				char licKarty = 'A';
				Random generatorCisel = new Random();
				while (volnePozice.Count >= 2) {
					int cislo = generatorCisel.Next(volnePozice.Count);
					Pozice prvniKarta = (Pozice) volnePozice[cislo];
					volnePozice.Remove(prvniKarta);

					cislo = generatorCisel.Next(volnePozice.Count);
					Pozice druhaKarta = (Pozice) volnePozice[cislo];
					volnePozice.Remove(druhaKarta);

					HerniPole[prvniKarta.X, prvniKarta.Y] = licKarty;
					HerniPole[druhaKarta.X, druhaKarta.Y] = licKarty;
					
					licKarty++;
				}
				for (int i = 0; i < mezeryPozice.Count; i++) {
					Pozice prazdnaKarta = (Pozice) mezeryPozice[i];
					HerniPole[prazdnaKarta.X, prazdnaKarta.Y] = '0';
				}
			}
			
			public void VykresliPlochu() {
				Console.Clear();
				for (int y = 0; y < RozmerPouzeKaret; y++) {
					for (int x = 0; x < Rozmer; x++) {
						switch(PoleStavu[x, y]) {
							case StavKarty.Skryta:
								Console.ForegroundColor = ConsoleColor.Green;
								Console.Write(RubKarty);
								Console.ResetColor();
								break;
							case StavKarty.Odstranena:
								Console.Write(' ');
								break;
							case StavKarty.Otocena:
								Console.Write(HerniPole[x,y]);
								break;
							case StavKarty.Mezera:
								Console.Write(' ');
								break;
						}
					}
					Console.WriteLine();
				}
				Console.WriteLine();
				Console.ForegroundColor = hrac1.Barva;
				Console.Write("{0}", hrac1.Jmeno);
				Console.ResetColor();
				Console.Write(" má skóre {0}\n", hrac1.Skore);
				Console.ForegroundColor = hrac2.Barva;
				Console.Write("{0}", hrac2.Jmeno);
				Console.ResetColor();
				Console.Write(" má skóre {0}\n", hrac2.Skore);
			}
			
			public bool VseOdhaleno() {
				return OdhaleneKarty == RozmerPouzeKaret * RozmerPouzeKaret;
			}
			
			public bool VyberKartu(ConsoleColor barva, bool prvniKarta) {
	
				int poziceXKurzoru = 0;
				int poziceYKurzoru = 0;
				int predchoziPoziceXKurzoru = 0;
				int predchoziPoziceYKurzoru = 0;
				for (int y = 0; y < RozmerPouzeKaret; y++) {
					for (int x = 0; x < Rozmer; x++) {
						if (PoleStavu[x,y] == StavKarty.Skryta) {
							Console.SetCursorPosition(x, y);
							poziceXKurzoru = x;
							poziceYKurzoru = y;
							Console.ForegroundColor = barva;
							Console.Write(RubKarty);
							Console.ResetColor();
							goto Navesti1;
						}
					}
				}
			Navesti1:
				//Console.SetCursorPosition();
				bool vybranaKarta = false;
				while (!vybranaKarta) {
					ConsoleKeyInfo zmacknutaKlavesa = Console.ReadKey(true);
					switch (zmacknutaKlavesa.Key) {
					case ConsoleKey.UpArrow:
						if (poziceYKurzoru > 0) {
							predchoziPoziceYKurzoru = poziceYKurzoru;
							predchoziPoziceXKurzoru = poziceXKurzoru;
							poziceYKurzoru--;
							
							OdeberOznaceniNaPredchoziPoziciKurzoru(predchoziPoziceXKurzoru, predchoziPoziceYKurzoru);
							
							PridejOznaceniNaAktualniPoziciKurzoru(poziceXKurzoru, poziceYKurzoru, barva);
						}
						break;
					case ConsoleKey.DownArrow:
						if (poziceYKurzoru < (RozmerPouzeKaret - 1)) {
							predchoziPoziceYKurzoru = poziceYKurzoru;
							predchoziPoziceXKurzoru = poziceXKurzoru;
							poziceYKurzoru++;
							
							OdeberOznaceniNaPredchoziPoziciKurzoru(predchoziPoziceXKurzoru, predchoziPoziceYKurzoru);
							
							PridejOznaceniNaAktualniPoziciKurzoru(poziceXKurzoru, poziceYKurzoru, barva);
						}
						break;
					case ConsoleKey.LeftArrow:
						if (poziceXKurzoru > 0) {
							predchoziPoziceYKurzoru = poziceYKurzoru;
							predchoziPoziceXKurzoru = poziceXKurzoru;
							poziceXKurzoru += -2;
							
							OdeberOznaceniNaPredchoziPoziciKurzoru(predchoziPoziceXKurzoru, predchoziPoziceYKurzoru);
							
							PridejOznaceniNaAktualniPoziciKurzoru(poziceXKurzoru, poziceYKurzoru, barva);
						}
						break;
					case ConsoleKey.RightArrow:
						if (poziceXKurzoru < (Rozmer - 1)) {
							predchoziPoziceYKurzoru = poziceYKurzoru;
							predchoziPoziceXKurzoru = poziceXKurzoru;
							poziceXKurzoru += 2;
							
							OdeberOznaceniNaPredchoziPoziciKurzoru(predchoziPoziceXKurzoru, predchoziPoziceYKurzoru);
							
							PridejOznaceniNaAktualniPoziciKurzoru(poziceXKurzoru, poziceYKurzoru, barva);
						}
						break;
					case ConsoleKey.Enter:
						if (PoleStavu[poziceXKurzoru,poziceYKurzoru] == StavKarty.Skryta) {
							vybranaKarta = true;	
							if (prvniKarta) {
								OtocPrvnikartu(poziceXKurzoru,poziceYKurzoru);
							}
							else {
								return OtocDruhoukartu(poziceXKurzoru,poziceYKurzoru);
							}
						}
						break;
					default:
						continue;
					}
				}
				return false;
			}
				
			public void PridejOznaceniNaAktualniPoziciKurzoru(int poziceXKurzoru, int poziceYKurzoru, ConsoleColor barva) {
					Console.SetCursorPosition(poziceXKurzoru, poziceYKurzoru);
							Console.ForegroundColor = barva;
							switch (PoleStavu[poziceXKurzoru, poziceYKurzoru]) {
								case StavKarty.Skryta:
									Console.Write(RubKarty);
									break;
								case StavKarty.Otocena:
									Console.Write(HerniPole[poziceXKurzoru, poziceYKurzoru]);
									break;
								case StavKarty.Odstranena:
									Console.BackgroundColor = barva;
									Console.Write(' ');
									break;
							}
							Console.ResetColor();
				}
				
			public void OdeberOznaceniNaPredchoziPoziciKurzoru(int predchoziPoziceXKurzoru, int predchoziPoziceYKurzoru) {
					Console.SetCursorPosition(predchoziPoziceXKurzoru, predchoziPoziceYKurzoru);
							switch (PoleStavu[predchoziPoziceXKurzoru, predchoziPoziceYKurzoru]) {
								case StavKarty.Skryta:
									Console.ForegroundColor = ConsoleColor.Green;
									Console.Write(RubKarty);
									Console.ResetColor();
									break;
								case StavKarty.Otocena:
									Console.Write(HerniPole[predchoziPoziceXKurzoru, predchoziPoziceYKurzoru]);
									break;
								case StavKarty.Odstranena:
									Console.Write(' ');
									break;
							}
				}
			
			public void OtocPrvnikartu(int x,int y) {
				OtocenaKarta = new Pozice(x, y);
				PoleStavu[x,y] = StavKarty.Otocena;
				VykresliPlochu();
			}
			
			public bool OtocDruhoukartu(int x,int y) {
				PoleStavu[x,y] = StavKarty.Otocena;
				VykresliPlochu();
				Thread.Sleep(2000);
				//Console.ReadKey(true);
				if (HerniPole[OtocenaKarta.X, OtocenaKarta.Y] == HerniPole[x,y]) {
					PoleStavu[OtocenaKarta.X, OtocenaKarta.Y] = StavKarty.Odstranena;
					PoleStavu[x, y] = StavKarty.Odstranena;
					OdhaleneKarty += 2;
					aktualniHrac = Program.aktualniHrac;
					aktualniHrac.Skore++;
					if (!VseOdhaleno()) {
						return true;
					}
					else {
						return false;
					}
				}
				else {
					PoleStavu[OtocenaKarta.X, OtocenaKarta.Y] = StavKarty.Skryta;
					PoleStavu[x, y] = StavKarty.Skryta;
					return false;
				}
			}
	}
	
	class Program
	{
		static HerniObrazovka herniObrazovka;
		public static Hrac aktualniHrac; 
		
		public static Hrac[] hraci = new Hrac[2];
		
		public static void Main(string[] args)
		{
			Console.WriteLine("Vítejte v mini pexesu!");
			
			bool platneJmenoHrace = false;
			while (!platneJmenoHrace) {
				Console.WriteLine("Zadejte jméno prvního hráče: ");
				string jmeno1 = Console.ReadLine();
				Console.WriteLine("Zadejte jméno druhého hráče: ");
				string jmeno2 = Console.ReadLine();
				
				if (jmeno1 != "" && jmeno2 != "") {
					platneJmenoHrace = true;
					hraci[0] = new Hrac(1, jmeno1, 0, ConsoleColor.Red);
					hraci[1] = new Hrac(2, jmeno2, 0, ConsoleColor.Blue);
				}
			}

			Console.WriteLine("Stiskněte \"a\" pro standartni hru");
			Console.WriteLine("Stiskněte \"b\" pro vlastní hru");
			Console.WriteLine("Stiskněte cokoliv jiného pro ukončení hry");
			ConsoleKeyInfo Klavesa = Console.ReadKey(true);
			
			switch (Klavesa.Key) {
				case ConsoleKey.A:
					herniObrazovka = new HerniObrazovka();
					break;
				case ConsoleKey.B:
					break;
				default:
					return;
			}
			while (!herniObrazovka.VseOdhaleno()) {
				herniObrazovka.VykresliPlochu();
				bool hracHrajeJesteJednou = false;
				
				do {
				aktualniHrac = hraci[0];
				herniObrazovka.VyberKartu(hraci[0].Barva, true);
				hracHrajeJesteJednou = herniObrazovka.VyberKartu(hraci[0].Barva, false);	
				}while(hracHrajeJesteJednou);
				hracHrajeJesteJednou = false;
				if (herniObrazovka.VseOdhaleno())
					continue;
				herniObrazovka.VykresliPlochu();

				do {
				aktualniHrac = hraci[1];
				herniObrazovka.VyberKartu(hraci[1].Barva, true);
				hracHrajeJesteJednou = herniObrazovka.VyberKartu(hraci[1].Barva, false);
				}while(hracHrajeJesteJednou);
				
			}
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine();
			if (hraci[0].Skore > hraci[1].Skore) {
				Console.WriteLine("{0} Vyhrál! Congratulations!", hraci[0].Jmeno);
			}
			else if (hraci[0].Skore < hraci[1].Skore){
				Console.WriteLine("{0} Vyhrál! Congratulations!", hraci[1].Jmeno);
			}
			else {
				Console.WriteLine("Oba hráči mají stejný počet bodů! Congratulations!");
			}
			Console.ResetColor();
			Console.ReadKey();
		}
	}
	
	class Hrac {
		
		public string Jmeno;
		public int Skore;
		public ConsoleColor Barva;
		public int cisloHrace;
	
		public Hrac(int cisloHrace, string jmeno, int skore, ConsoleColor Barva) {
			this.cisloHrace = cisloHrace;
			this.Jmeno = jmeno;
			this.Skore = skore;
			this.Barva = Barva;
		}
		
	}
}