﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 28.7.2012
 * Čas: 16:14
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.Collections;

namespace Adresar
{
	static class HlavniNabidka {
		
		public static int ZobrazHlavniNabidku() {
			
			string[] polozkyNabidky = new string[6];
			polozkyNabidky[0] = "Přidání záznamu";
			polozkyNabidky[1] = "Výpis záznamů";
			polozkyNabidky[2] = "Úprava záznamu";
			polozkyNabidky[3] = "Smazání záznamu";
			polozkyNabidky[4] = "Hledání";
			polozkyNabidky[5] = "Konec";
			
			int zvolenaPolozka = 0;
			bool vyberDokoncen = false;
			
			Console.Clear();
			
			Console.WriteLine("Národní registr občanů s IQ > 120");
			Console.WriteLine();
			
			while (!vyberDokoncen) {
				Console.SetCursorPosition(0,3);
				for (int i = 0; i < polozkyNabidky.Length ;i++) {
					if (zvolenaPolozka == i) {
						Console.BackgroundColor = ConsoleColor.Blue;
					}
					Console.WriteLine(polozkyNabidky[i]);
					Console.ResetColor();
				}
				ConsoleKeyInfo StisknutaKlavesa = Console.ReadKey(true);
				if (StisknutaKlavesa.Key == ConsoleKey.DownArrow) {
					zvolenaPolozka++;
					if (zvolenaPolozka == polozkyNabidky.Length) {
						zvolenaPolozka = 0;
					}
				}
				else if (StisknutaKlavesa.Key == ConsoleKey.UpArrow) {
					zvolenaPolozka--;
					if (zvolenaPolozka < 0) {
						zvolenaPolozka = polozkyNabidky.Length - 1;
					}
				}
				else if (StisknutaKlavesa.Key == ConsoleKey.Enter) {
					vyberDokoncen = true;
				}
			}
			return zvolenaPolozka;
		}
	}
	
	class Program
	{
		
		const int PridaniZaznamu = 0;
		const int VypisZaznamu = 1;
		const int UpravaZaznamu = 2;
		const int SmazaniZaznamu = 3;
		const int Hledani = 4;
		const int Konec = 5;
		
		static void UpravZaznam(Zaznam zaznam) {
			Console.WriteLine("Zadejte prosím jméno");
			zaznam.Jmeno = Console.ReadLine();
			
			Console.WriteLine("Zadejte prosím adresu");
			zaznam.Adresa = Console.ReadLine();
		}
		 
		static void VypisZaznam(Zaznam zaznam) {
			Console.WriteLine("Jméno: {0}\nAdresa: {1}",zaznam.Jmeno, zaznam.Adresa);
		}
		
		public static void Main(string[] args)
		{
			ArrayList seznamZaznamu = new ArrayList();
			bool programBezi = true;
			while(programBezi) {
				int vysledekVolby = HlavniNabidka.ZobrazHlavniNabidku();
				switch (vysledekVolby) {
					case PridaniZaznamu:
						Console.Clear();
						Zaznam novyZaznam = new Zaznam();
						UpravZaznam(novyZaznam);
						Console.WriteLine("Záznam úspěšně přidán! Číslo záznamu: " + seznamZaznamu.Add(novyZaznam));
						Console.ReadKey();
						break;
					case VypisZaznamu:
						Console.Clear();
						if (seznamZaznamu.Count > 0) {
							int pocitadlo = 0;
							foreach (object o in seznamZaznamu) {
								if (o is Zaznam) {
									Zaznam zaznam = (Zaznam) o;
									Console.WriteLine();
									Console.WriteLine("Číslo záznamu: {0}", pocitadlo++);
									VypisZaznam(zaznam);
								}
							}
						}
						else {
							Console.WriteLine("Seznam je prázdný!");
						}
						Console.ReadKey();
						break;
					case UpravaZaznamu:
						Console.WriteLine();
						Console.WriteLine("Zadejte prosím číslo záznamu:");
						try {
							int cisloZaznamu = int.Parse(Console.ReadLine());
							Zaznam vybranyZaznam = (Zaznam) seznamZaznamu[cisloZaznamu];
							Console.WriteLine();
							VypisZaznam(vybranyZaznam);
							Console.WriteLine();
							UpravZaznam(vybranyZaznam);
						}
						catch(FormatException) {
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Neplatný vstup!");
							Console.ReadKey();
						}
						catch(ArgumentOutOfRangeException) {
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Záznam na dané pozici neexistuje!");
							Console.ReadKey();
						}
						catch(OverflowException) {
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Zadaná hodnota je mimo rozsah!");
							Console.ReadKey();
						}
						Console.ResetColor();
						break;
					case SmazaniZaznamu: 
						Console.WriteLine();
						Console.WriteLine("Zadejte prosím číslo záznamu:");
						try {
							int cisloZaznamu = int.Parse(Console.ReadLine());
							Zaznam vybranyZaznam = (Zaznam) seznamZaznamu[cisloZaznamu];
							seznamZaznamu.RemoveAt(cisloZaznamu);
						}
						catch(FormatException) {
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Neplatný vstup!");
							Console.ReadKey();
						}
						catch(ArgumentOutOfRangeException) {
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Záznam na dané pozici neexistuje!");
							Console.ReadKey();
						}
						catch(OverflowException) {
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Zadaná hodnota je mimo rozsah!");
							Console.ReadKey();
						}
						Console.ResetColor();
						break;
					case Hledani:
						Console.WriteLine();
						Console.WriteLine("Zadejte prosím hledané jméno nebo jeho část:");
						string hledanyRetezec = Console.ReadLine();
						int nalezeneZaznamy = 0;
						int pocitadlo1 = 0;
						foreach (object o in seznamZaznamu) {
							if (o is Zaznam) {
								Zaznam zaznam = (Zaznam) o;
								if (zaznam.Jmeno.Contains(hledanyRetezec)) {
									Console.WriteLine();
									Console.WriteLine("Číslo záznamu: {0}", pocitadlo1++);
									VypisZaznam(zaznam);
									nalezeneZaznamy++;
								}
							}
						}
						if (nalezeneZaznamy > 0) {
							Console.WriteLine();
							Console.WriteLine("Počet nalezených záznamů: {0}", nalezeneZaznamy);
						}
						else {
							Console.WriteLine();
							Console.WriteLine("Nebyl nalezen žádný záznam");
						}
						Console.ReadKey();
						break;
					case Konec:
						programBezi = false;
						break;
				}
			}
		}
	}
}