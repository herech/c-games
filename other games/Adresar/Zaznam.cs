﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 28.7.2012
 * Čas: 16:15
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;

namespace Adresar
{
	/// <summary>
	/// Description of Zaznam.
	/// </summary>
	public class Zaznam
	{
		private string jmeno;
		public string Jmeno {
			get {
				return jmeno;
			}
			set {
				jmeno = value;
			}
		}
		
		private string adresa;
		public string Adresa {
			get {
				return adresa;
			}
			set {
				adresa = value;
			}
		}
		public Zaznam()
		{
		}
	}
}
