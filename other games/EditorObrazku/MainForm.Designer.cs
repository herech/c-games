﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 31.7.2012
 * Čas: 17:27
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
namespace EditorObrazku
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tlacitko_Ulozit = new System.Windows.Forms.Button();
			this.tlacitko_Otevrit = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.posuvnik = new System.Windows.Forms.TrackBar();
			this.seznamEfektu = new System.Windows.Forms.ComboBox();
			this.tlacitko_Proved = new System.Windows.Forms.Button();
			this.zpet = new System.Windows.Forms.Button();
			this.vpred = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.posuvnik)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tlacitko_Ulozit);
			this.groupBox1.Controls.Add(this.tlacitko_Otevrit);
			this.groupBox1.Location = new System.Drawing.Point(1017, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(154, 128);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Soubor";
			// 
			// tlacitko_Ulozit
			// 
			this.tlacitko_Ulozit.Enabled = false;
			this.tlacitko_Ulozit.Location = new System.Drawing.Point(19, 82);
			this.tlacitko_Ulozit.Name = "tlacitko_Ulozit";
			this.tlacitko_Ulozit.Size = new System.Drawing.Size(115, 29);
			this.tlacitko_Ulozit.TabIndex = 1;
			this.tlacitko_Ulozit.Text = "Uložit";
			this.tlacitko_Ulozit.UseVisualStyleBackColor = true;
			this.tlacitko_Ulozit.Click += new System.EventHandler(this.Tlacitko_UlozitClick);
			// 
			// tlacitko_Otevrit
			// 
			this.tlacitko_Otevrit.Location = new System.Drawing.Point(19, 35);
			this.tlacitko_Otevrit.Name = "tlacitko_Otevrit";
			this.tlacitko_Otevrit.Size = new System.Drawing.Size(115, 31);
			this.tlacitko_Otevrit.TabIndex = 0;
			this.tlacitko_Otevrit.Text = "Otevřít";
			this.tlacitko_Otevrit.UseVisualStyleBackColor = true;
			this.tlacitko_Otevrit.Click += new System.EventHandler(this.Tlacitko_OtevritClick);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.posuvnik);
			this.groupBox2.Controls.Add(this.seznamEfektu);
			this.groupBox2.Controls.Add(this.tlacitko_Proved);
			this.groupBox2.Location = new System.Drawing.Point(1017, 156);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(154, 212);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Efekty";
			// 
			// posuvnik
			// 
			this.posuvnik.Location = new System.Drawing.Point(19, 70);
			this.posuvnik.Maximum = 50;
			this.posuvnik.Name = "posuvnik";
			this.posuvnik.Size = new System.Drawing.Size(115, 45);
			this.posuvnik.TabIndex = 2;
			this.posuvnik.TickFrequency = 5;
			this.posuvnik.Visible = false;
			// 
			// seznamEfektu
			// 
			this.seznamEfektu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.seznamEfektu.Enabled = false;
			this.seznamEfektu.FormattingEnabled = true;
			this.seznamEfektu.Items.AddRange(new object[] {
									"Invertovat Barvy",
									"Rozmazat",
									"Zesvětlit",
									"Ztmavit",
									"Otočit vodorovně",
									"Otočit svisle",
									"Vlastní efekt",
									"Vlastní efekt1",
									"Vlastní efekt2"});
			this.seznamEfektu.Location = new System.Drawing.Point(19, 121);
			this.seznamEfektu.Name = "seznamEfektu";
			this.seznamEfektu.Size = new System.Drawing.Size(115, 21);
			this.seznamEfektu.TabIndex = 1;
			this.seznamEfektu.SelectedIndexChanged += new System.EventHandler(this.SeznamEfektuSelectedIndexChanged);
			// 
			// tlacitko_Proved
			// 
			this.tlacitko_Proved.Enabled = false;
			this.tlacitko_Proved.Location = new System.Drawing.Point(19, 34);
			this.tlacitko_Proved.Name = "tlacitko_Proved";
			this.tlacitko_Proved.Size = new System.Drawing.Size(114, 30);
			this.tlacitko_Proved.TabIndex = 0;
			this.tlacitko_Proved.Text = "Proveď";
			this.tlacitko_Proved.UseVisualStyleBackColor = true;
			this.tlacitko_Proved.Click += new System.EventHandler(this.Tlacitko_ProvedClick);
			// 
			// zpet
			// 
			this.zpet.Enabled = false;
			this.zpet.Location = new System.Drawing.Point(17, 35);
			this.zpet.Name = "zpet";
			this.zpet.Size = new System.Drawing.Size(55, 31);
			this.zpet.TabIndex = 2;
			this.zpet.Text = "Zpět";
			this.zpet.UseVisualStyleBackColor = true;
			this.zpet.Click += new System.EventHandler(this.ZpetClick);
			// 
			// vpred
			// 
			this.vpred.Enabled = false;
			this.vpred.Location = new System.Drawing.Point(77, 35);
			this.vpred.Name = "vpred";
			this.vpred.Size = new System.Drawing.Size(54, 31);
			this.vpred.TabIndex = 3;
			this.vpred.Text = "Vpřed";
			this.vpred.UseVisualStyleBackColor = true;
			this.vpred.Click += new System.EventHandler(this.VpredClick);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.zpet);
			this.groupBox3.Controls.Add(this.vpred);
			this.groupBox3.Location = new System.Drawing.Point(1017, 385);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(152, 97);
			this.groupBox3.TabIndex = 4;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Úpravy";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1194, 668);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "Editor Obrázků";
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainFormPaint);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.posuvnik)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button vpred;
		private System.Windows.Forms.Button zpet;
		private System.Windows.Forms.TrackBar posuvnik;
		private System.Windows.Forms.Button tlacitko_Proved;
		private System.Windows.Forms.ComboBox seznamEfektu;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button tlacitko_Otevrit;
		private System.Windows.Forms.Button tlacitko_Ulozit;
		private System.Windows.Forms.GroupBox groupBox1;
	}
}
