﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 31.7.2012
 * Čas: 17:27
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Collections;

namespace EditorObrazku
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form 
	{
		string jmenoObrazku = "";
		Bitmap Obrazek;
		Color[,] Barvy;
		ArrayList seznamKroku = new ArrayList();
		int indexPosledniUpravy = 0;
		bool provedenaAkce = false;
		bool zmacknutoZpet = false;
		
		delegate void DelegatZvolenyEfekt();
		DelegatZvolenyEfekt zvolenyEfekt;
		Random rand = new Random();
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		private void OtevriSoubor() {
			OpenFileDialog otevreniSouboru = new OpenFileDialog();
			otevreniSouboru.Filter = "Obrázky (*.bmp, *.jpg)|*.bmp; *.jpg";
			otevreniSouboru.ShowDialog();
			this.jmenoObrazku = otevreniSouboru.FileName;
			if (jmenoObrazku != "") {
				using (Bitmap docasnyObrazek = (Bitmap) Bitmap.FromFile(otevreniSouboru.FileName)) {
					if (docasnyObrazek.Width <= 1000 && docasnyObrazek.Height <= 680) {
						if (Obrazek != null) {
							Obrazek.Dispose();
							Obrazek = null;
						}
						Obrazek = (Bitmap) docasnyObrazek.Clone();
						Barvy = new Color[Obrazek.Width, Obrazek.Height];
						for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
							for (int radek = 0; radek < Obrazek.Height;radek++) {
								Barvy[sloupec, radek] = Obrazek.GetPixel(sloupec, radek);
							}
						}
						this.seznamEfektu.Enabled = true;
						this.Invalidate();
					}
					else {
						MessageBox.Show("Obrázek je příliš velký!", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				this.indexPosledniUpravy = seznamKroku.Add(Obrazek.Clone());
			}
		}
		
		private void InverzeBarev() {
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					Color docasnaBarva = Barvy[sloupec, radek];
					int novaCervena = 255 - docasnaBarva.R;
					int novaZelena = 255 - docasnaBarva.G;
					int novaModra = 255 - docasnaBarva.B;
					Color invertovanaBarva = Color.FromArgb(novaCervena, novaZelena, novaModra);
					Obrazek.SetPixel(sloupec, radek, invertovanaBarva);
					Barvy[sloupec, radek] = invertovanaBarva;
				}
			}
			this.Invalidate();
		}
		
		private void ZesvetliObrazek(int hodnota) {
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					Color docasnaBarva = Barvy[sloupec, radek];
					int novaCervena = Math.Min(docasnaBarva.R+hodnota,255);
					int novaZelena = Math.Min(docasnaBarva.G+hodnota,255);
					int novaModra = Math.Min(docasnaBarva.B+hodnota,255);
					Color svetlejsiBarva = Color.FromArgb(novaCervena, novaZelena, novaModra);
					Obrazek.SetPixel(sloupec, radek, svetlejsiBarva);
					Barvy[sloupec, radek] = svetlejsiBarva;
				}
			}
			this.Invalidate();
		}
		
		private void ZtmavObrazek(int hodnota) {
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					Color docasnaBarva = Barvy[sloupec, radek];
					int novaCervena = Math.Max(docasnaBarva.R-hodnota,0);
					int novaZelena = Math.Max(docasnaBarva.G-hodnota,0);
					int novaModra = Math.Max(docasnaBarva.B-hodnota,0);
					Color ztmavenaBarva = Color.FromArgb(novaCervena, novaZelena, novaModra);
					Obrazek.SetPixel(sloupec, radek, ztmavenaBarva);
					Barvy[sloupec, radek] = ztmavenaBarva;
				}
			}
			this.Invalidate();
		}
		
		private void RozmazObrazek() {
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					Color barva;
					if (sloupec > 0) {
						barva = Barvy[sloupec - 1, radek];
					}
					else {
						barva = Barvy[sloupec, radek];
					}
					int predchoziCervena = barva.R;
					int predchoziZelena = barva.G;
					int predchoziModra = barva.B;
					
					if (sloupec < Obrazek.Width - 1) {
						barva = Barvy[sloupec + 1, radek];
					}
					else {
						barva = Barvy[sloupec, radek];
					}
					
					int nasledujiciCervena = barva.R;
					int nasledujiciZelena = barva.G;
					int nasledujiciModra = barva.B;
					
					int novaCervena = (predchoziCervena + nasledujiciCervena) / 2;
					int novaZelena = (predchoziZelena + nasledujiciZelena) / 2;
					int novaModra = (predchoziModra + nasledujiciModra) / 2;
					
					Color novaBarva = Color.FromArgb(novaCervena, novaZelena, novaModra);
					Obrazek.SetPixel(sloupec, radek, novaBarva);
					Barvy[sloupec, radek] = novaBarva;
				}
			}
			this.Invalidate();
		}
		
		private void OtocVodorovne() {
			Color[,] docasnePole = new Color[Obrazek.Width,Obrazek.Height];
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					Obrazek.SetPixel(sloupec, radek, Barvy[(Obrazek.Width - 1) - sloupec, radek]);
					docasnePole[sloupec, radek] = Barvy[(Obrazek.Width - 1) - sloupec, radek];
				}
			}
			Barvy = docasnePole;
			this.Invalidate();
		}
		
		private void OtocSvisle() {
			Color[,] docasnePole = new Color[Obrazek.Width,Obrazek.Height];
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					Obrazek.SetPixel(sloupec, radek, Barvy[sloupec, (Obrazek.Height - 1) - radek]);
					docasnePole[sloupec, radek] = Barvy[sloupec, (Obrazek.Height - 1) - radek];
				}
			}
			Barvy = docasnePole;
			this.Invalidate();
		}
		
		private void VlastniEfekt() {
			Color[,] docasnePole = new Color[Obrazek.Width,Obrazek.Height];
			int nahodencislo1 = 0;
			int nahodencislo2 = 0;
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					if (sloupec > 3 && sloupec < Obrazek.Width - 4 && radek > 3 && radek < Obrazek.Height - 4) {
						nahodencislo1 = rand.Next(sloupec - 3, sloupec + 3);
						nahodencislo2 = rand.Next(radek - 3, radek + 3);
						Obrazek.SetPixel(sloupec, radek, Barvy[nahodencislo1, nahodencislo2]);
						docasnePole[sloupec, radek] = Barvy[nahodencislo1, nahodencislo2];
					}
					else {
						Obrazek.SetPixel(sloupec, radek, Barvy[sloupec, radek]);
						docasnePole[sloupec, radek] = Barvy[sloupec, radek];
					}
				}
			}
			Barvy = docasnePole;
			this.Invalidate();
		}
		private void VlastniEfekt1(int stupen) {
			Color[,] docasnePole = new Color[Obrazek.Width,Obrazek.Height];
			stupen *= 2;
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					Color barva;
						barva = Barvy[sloupec, radek];
					
					int predchoziCervena = barva.R;
					int predchoziZelena = barva.G;
					int predchoziModra = barva.B;
					int novaCervena = 0;
					int novaZelena = 0;
					int novaModra = 0;
					
					if (predchoziCervena < 150 && predchoziZelena < 150 && predchoziModra < 150) {
						novaCervena = Math.Max(predchoziCervena - stupen,0);
						novaZelena = Math.Max(predchoziZelena - stupen,0);
						novaModra = Math.Max(predchoziModra - stupen,0);
					}
					else {
						novaCervena = Math.Min(predchoziCervena + stupen,255);
						novaZelena = Math.Min(predchoziZelena + stupen,255);
						novaModra = Math.Min(predchoziModra + stupen,255);
					}
					
					Color novaBarva = Color.FromArgb(novaCervena, novaZelena, novaModra);
					Obrazek.SetPixel(sloupec, radek, novaBarva);
					docasnePole[sloupec, radek] = novaBarva;
				}
			}
			Barvy = docasnePole;
			this.Invalidate();
		}
		
		private void VlastniEfekt2(int stupen) {
			Color[,] docasnePole = new Color[Obrazek.Width,Obrazek.Height];
			for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
				for (int radek = 0; radek < Obrazek.Height;radek++) {
					Color barva;
						barva = Barvy[sloupec, radek];
					
					int predchoziCervena = barva.R;
					int predchoziZelena = barva.G;
					int predchoziModra = barva.B;
					int novaCervena = 0;
					int novaZelena = 0;
					int novaModra = 0;
					
					/*if (true) {
						if (sloupec > 0) {
							barva = Barvy[sloupec - 1, radek];
						}
						else {
							barva = Barvy[sloupec, radek];
						}
						predchoziCervena = barva.R;
					 	predchoziZelena = barva.G;
						predchoziModra = barva.B;
						
						if (sloupec < Obrazek.Width - 1) {
							barva = Barvy[sloupec + 1, radek];
						}
						else {
							barva = Barvy[sloupec, radek];
						}
						
						int nasledujiciCervena = barva.R;
						int nasledujiciZelena = barva.G;
						int nasledujiciModra = barva.B;
						
						novaCervena = (predchoziCervena + nasledujiciCervena) / 3;
						novaZelena = predchoziZelena;
						novaModra = (predchoziModra + nasledujiciModra) / 3;
						
					}*/
					
						if (predchoziModra > predchoziCervena && predchoziModra > predchoziZelena) {
							//stupen += stupen;
							novaCervena = Math.Max(predchoziCervena - stupen,0);
							novaZelena = Math.Max(predchoziZelena - stupen,0);;
							novaModra = Math.Min(predchoziModra + stupen,255);
						}
						if (predchoziModra < 100 && predchoziZelena < 100 && predchoziCervena < 100) {
							//stupen += stupen;
							novaCervena = Math.Max(predchoziCervena - stupen,0);
							novaZelena = Math.Max(predchoziZelena - stupen,0);
							novaModra = Math.Max(predchoziModra - stupen,0);
						}
						else {
							novaCervena = Math.Min(predchoziCervena + stupen,255);
							novaZelena = Math.Min(predchoziZelena + stupen,255);
							novaModra = Math.Min(predchoziModra + stupen,255);
						}
					
					
					
					Color novaBarva = Color.FromArgb(novaCervena, novaZelena, novaModra);
					Obrazek.SetPixel(sloupec, radek, novaBarva);
					docasnePole[sloupec, radek] = novaBarva;
				}
			}
			Barvy = docasnePole;
			this.Invalidate();
		}
		
		private void Tlacitko_OtevritClick(object sender, EventArgs e)
		{
			OtevriSoubor();
		}
		
		private void MainFormPaint(object sender, PaintEventArgs e)
		{
			if (Obrazek != null) {
				e.Graphics.DrawImage(Obrazek, 10,10);
			}
		}
		
		private void SeznamEfektuSelectedIndexChanged(object sender, EventArgs e)
		{
			this.tlacitko_Proved.Enabled = true;
			switch (seznamEfektu.SelectedIndex) {
				case 0:
					zvolenyEfekt = this.InverzeBarev;
					this.posuvnik.Visible = false;
					break;
				case 1:
					zvolenyEfekt = this.RozmazObrazek;
					this.posuvnik.Visible = false;
					break;
				case 2:
					zvolenyEfekt = () => this.ZesvetliObrazek(posuvnik.Value);
					this.posuvnik.Visible = true;
					break;
				case 3:
					zvolenyEfekt = () => this.ZtmavObrazek(posuvnik.Value);
					this.posuvnik.Visible = true;
					break;
				case 4:
					zvolenyEfekt = this.OtocVodorovne;
					this.posuvnik.Visible = false;
					break;
				case 5:
					zvolenyEfekt = this.OtocSvisle;
					this.posuvnik.Visible = false;
					break;
				case 6:
					zvolenyEfekt = this.VlastniEfekt;
					this.posuvnik.Visible = false;
					break;
				case 7:
					zvolenyEfekt = () => this.VlastniEfekt1(posuvnik.Value);
					this.posuvnik.Visible = true;
					break;
				case 8:
					zvolenyEfekt = () => this.VlastniEfekt2(posuvnik.Value);
					this.posuvnik.Visible = true;
					break;
			}
		}
		
		private void Tlacitko_ProvedClick(object sender, EventArgs e)
		{
			if (this.zvolenyEfekt != null) {
				this.zvolenyEfekt();
				this.tlacitko_Ulozit.Enabled = true;
				this.zpet.Enabled = true;
				this.vpred.Enabled = false;
				this.provedenaAkce = true;
				if (seznamKroku.Count > 10) {
					seznamKroku.RemoveAt(0);
				}
				if (zmacknutoZpet) {
					int pocetSezanamKroku = seznamKroku.Count;
					for (int i = pocetSezanamKroku - 1;i > indexPosledniUpravy;i--) {
						seznamKroku.RemoveAt(i);
						//MessageBox.Show("počet: " + seznamKroku.Count.ToString() + " aktualni index: " + i, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				this.indexPosledniUpravy = seznamKroku.Add(Obrazek.Clone());
				zmacknutoZpet = false;
				//MessageBox.Show(seznamKroku.Count.ToString(), "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
		private void Tlacitko_UlozitClick(object sender, EventArgs e)
		{
			try {
				if (Obrazek != null) {
					SaveFileDialog dialogUlozeni = new SaveFileDialog();
					dialogUlozeni.FileName = jmenoObrazku;
					dialogUlozeni.ShowDialog();
					string cesta = dialogUlozeni.FileName;
					if (cesta != "") {
						Obrazek.Save(cesta);
					}
				}
			}
			catch {
				MessageBox.Show("Chyba při ukládání souboru!", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
		private void ZpetClick(object sender, EventArgs e)
		{
			if (seznamKroku.Count > 0 && indexPosledniUpravy > 0) {
				zmacknutoZpet = true;
				indexPosledniUpravy--;
				Bitmap obr = (Bitmap) seznamKroku[indexPosledniUpravy];
				this.Obrazek = (Bitmap) obr.Clone();
				this.provedenaAkce = false;
				this.vpred.Enabled = true;
				for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
					for (int radek = 0; radek < Obrazek.Height;radek++) {
						Barvy[sloupec, radek] = Obrazek.GetPixel(sloupec, radek);
					}
				}
				this.Invalidate();
				/*Graphics grfx = CreateGraphics();
				grfx.DrawImage((Bitmap) seznamKroku[indexPosledniUpravy], 0,0);*/
				//MessageBox.Show(indexPosledniUpravy.ToString(), "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else {
				this.zpet.Enabled = false;
			}
		}
		
		private void VpredClick(object sender, EventArgs e)
		{
			if (!provedenaAkce && indexPosledniUpravy < seznamKroku.Count - 1) {
				indexPosledniUpravy++;
				Bitmap obr = (Bitmap) seznamKroku[indexPosledniUpravy];
				this.Obrazek = (Bitmap) obr.Clone();
				for (int sloupec = 0; sloupec < Obrazek.Width;sloupec++) {
					for (int radek = 0; radek < Obrazek.Height;radek++) {
						Barvy[sloupec, radek] = Obrazek.GetPixel(sloupec, radek);
					}
				}
				this.zpet.Enabled = true;
				this.Invalidate();
			}
			else {
				this.vpred.Enabled = false;
			}
		}
	}
}
