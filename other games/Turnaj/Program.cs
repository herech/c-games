﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 12.7.2012
 * Čas: 15:34
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;

namespace Turnaj
{
	class Postava {
		public string Jmeno;
		public int UtokZblizka = 0;
		public int UtokZdalky = 0;
		public int Uhybani = 0;
		public int Obrana = 0;
		public int Presnost = 0;
		public int Zivoty;
		protected Random nahodnaCisla;
		protected int FaktoryUtoku;
		
		public Postava(string jmeno, int zivoty, int utokZblizka, int utokZdalky, int obrana, int uhybani, int presnost) {
			Jmeno = jmeno;
			UtokZblizka = utokZblizka;
			UtokZdalky = utokZdalky;
			Uhybani = uhybani;
			Obrana = obrana;
			Presnost = presnost;
			Zivoty = zivoty;
			nahodnaCisla = new Random();
		}
		
		public void VypisAtributy() {
			Console.WriteLine("---------------");
			Console.WriteLine(Jmeno);
			Console.WriteLine("Útok z blízka: " + UtokZblizka);
			Console.WriteLine("Útok z dálky: " + UtokZdalky);
			Console.WriteLine("Uhýbání: " + Uhybani);
			Console.WriteLine("Obrana: " + Obrana);
			Console.WriteLine("Přesnost: " + Presnost);
			Console.WriteLine("Životy: " + Zivoty);
			Console.WriteLine("---------------");
		}
		
		public int SpocitejPrumernouObranu() {
			int vysledek = (Uhybani + Obrana) / 2;
			return vysledek;
		}
		
		public void ZmenZdravi() {
			Zivoty += nahodnaCisla.Next(20,40);
			/*	UtokZblizka += cisloKPricteni;
			UtokZdalky += cisloKPricteni;
			Uhybani += cisloKPricteni;
			Obrana += cisloKPricteni;
			Presnost += cisloKPricteni;*/
		}
		
		public int VypoctiUtok(int typUtoku) {
			switch (typUtoku) {
				case 1:
					FaktoryUtoku = UtokZblizka + Presnost / nahodnaCisla.Next(1, 5);
					return FaktoryUtoku - nahodnaCisla.Next((FaktoryUtoku / 5), (FaktoryUtoku / 3));
				case 2:
					FaktoryUtoku = UtokZdalky + Presnost / nahodnaCisla.Next(3, 6);
					return FaktoryUtoku - nahodnaCisla.Next((FaktoryUtoku / 4), (FaktoryUtoku / 2));
				default:
					return 0;
				
			}
		}
	}
	
	class Program
	{
		
		public static void Main(string[] args)
		{
			Postava[] nepratele = new Postava[4];
			
			Random nahodnaCisla = new Random();
			int faktorSpecifikace;
			faktorSpecifikace = nahodnaCisla.Next(1,3);
			switch (faktorSpecifikace) {
				case 1:
					nepratele[0] = new Postava("Strašidlo", nahodnaCisla.Next(50,80), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19), nahodnaCisla.Next(5,13), nahodnaCisla.Next(5,13), nahodnaCisla.Next(10,19));
					break;
				case 2:
					nepratele[0] = new Postava("Strašidlo", nahodnaCisla.Next(60,90), nahodnaCisla.Next(5,13), nahodnaCisla.Next(5,13), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19));
					break;
			}
			
			
			
			faktorSpecifikace = nahodnaCisla.Next(1,3);
			switch (faktorSpecifikace) {
				case 1:
					nepratele[1] = new Postava("Skřet", nahodnaCisla.Next(50,80), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19), nahodnaCisla.Next(5,13), nahodnaCisla.Next(5,13), nahodnaCisla.Next(10,19));
					break;
				case 2:
					nepratele[1] = new Postava("Skřet", nahodnaCisla.Next(60,90), nahodnaCisla.Next(5,13), nahodnaCisla.Next(5,13), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19));
					break;
			}
			
			
			faktorSpecifikace = nahodnaCisla.Next(1,3);
			switch (faktorSpecifikace) {
				case 1:
					nepratele[2] = new Postava("Zombie", nahodnaCisla.Next(50,80), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19), nahodnaCisla.Next(5,13), nahodnaCisla.Next(5,13), nahodnaCisla.Next(10,19));
					break;
				case 2:
					nepratele[2] = new Postava("Zombie", nahodnaCisla.Next(60,90), nahodnaCisla.Next(5,13), nahodnaCisla.Next(5,13), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19));
					break;
			}
			
			
			faktorSpecifikace = nahodnaCisla.Next(1,3);
			switch (faktorSpecifikace) {
				case 1:
					nepratele[3] = new Postava("Vkodlak", nahodnaCisla.Next(50,80), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19), nahodnaCisla.Next(5,13), nahodnaCisla.Next(5,13), nahodnaCisla.Next(10,19));
					break;
				case 2:
					nepratele[3] = new Postava("Vkodlak", nahodnaCisla.Next(60,90), nahodnaCisla.Next(5,13), nahodnaCisla.Next(5,13), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19), nahodnaCisla.Next(10,19));
					break;
			}
			
			
			
			short krokZlepseni = 5;
			Postava postavaHrace = new Postava("", 100, 0, 0, 0, 0, 0);
			Console.WriteLine("Vítej! Zadej prosím jméno postavy");
			postavaHrace.Jmeno = Console.ReadLine();
			Console.WriteLine("Vytvořte postavu rozdělením bodů mezi jednotlivé aributy");
			int bodyKrozdeleni = 45;
			
			while (bodyKrozdeleni > 0) {
				postavaHrace.VypisAtributy();
				Console.WriteLine("Zbývá rozdělit " + bodyKrozdeleni + " bodů");
				Console.WriteLine("Které dovednosti chceš zlepšit?");
				Console.WriteLine("Z pro útok z blízka");
				Console.WriteLine("D pro útok z dálky");
				Console.WriteLine("U pro uhýbání");
				Console.WriteLine("O pro obrana");
				Console.WriteLine("P pro přesnost");
				Console.WriteLine("Zvolte P/O/Z/D/U");
				
				switch(Console.ReadLine().ToUpper()) {
				case "P": 
					postavaHrace.Presnost += krokZlepseni;
					bodyKrozdeleni -= krokZlepseni;
					break;
				case "O": 
					postavaHrace.Obrana += krokZlepseni;
					bodyKrozdeleni -= krokZlepseni;
					break;
				case "Z": 
					postavaHrace.UtokZblizka += krokZlepseni;
					bodyKrozdeleni -= krokZlepseni;
					break;
				case "D": 
					postavaHrace.UtokZdalky += krokZlepseni;
					bodyKrozdeleni -= krokZlepseni;
					break;
				case "U": 
					postavaHrace.Uhybani += krokZlepseni;
					bodyKrozdeleni -= krokZlepseni;
					break;
				default:
					Console.WriteLine("Neplatný vstup");
					break;
				}
			}
			Console.WriteLine("Chcete zkusit štěstí a vypít neznámý lektvar? A=ano");
			Console.WriteLine("Vaši protivníci v turnaji: ");
			foreach (Postava jednotlivyNepritel in nepratele) {
				Console.WriteLine(jednotlivyNepritel.Jmeno + ": " + jednotlivyNepritel.Zivoty + " životů");
			}
			Console.WriteLine("---------------");
			Console.WriteLine("Turnaj začíná!");
			for (int i = 0; i < nepratele.Length; i++) {
				postavaHrace.Zivoty = 100;
				while ((postavaHrace.Zivoty > 0) && (nepratele[i].Zivoty > 0)) {
					postavaHrace.VypisAtributy();
					Console.WriteLine("PROTI");
					nepratele[i].VypisAtributy();
					
					string zadanyTypUtoku;
					do {
						Console.WriteLine("Zvolte útok (D = z dálky, Z = z blízka, nebo L = lektvar): ");
						zadanyTypUtoku = Console.ReadLine().ToUpper();
						if (zadanyTypUtoku == "L") {
							postavaHrace.ZmenZdravi();
							postavaHrace.VypisAtributy();
						}
					} while((zadanyTypUtoku != "D") && (zadanyTypUtoku != "Z"));
					
					int silaUtoku = 0, obrana = 0;
					switch (zadanyTypUtoku) {
						case "Z":
							silaUtoku = postavaHrace.VypoctiUtok(1);
							obrana = nepratele[i].Obrana;
							break;
						case "D":
							silaUtoku = postavaHrace.VypoctiUtok(2);
							obrana = nepratele[i].Uhybani;
							break;
					}
					
					if (silaUtoku > obrana) {
						Console.ForegroundColor = ConsoleColor.Red;
						Console.WriteLine("Ubral jsi protivníkovi " + (silaUtoku - obrana) + " životů");
						nepratele[i].Zivoty -= silaUtoku - obrana;
						if (nepratele[i].Zivoty <= 0) {
							Console.ForegroundColor = ConsoleColor.Blue;
							Console.WriteLine(postavaHrace.Jmeno + " porazil " + nepratele[i].Jmeno);
							break;
						}
						Console.ResetColor();
					}
					else {
						Console.ForegroundColor = ConsoleColor.Blue;
						Console.WriteLine("Nepřítel útok odrazil");
						Console.ResetColor();
					}
					
					int typUtokuNepritele = nahodnaCisla.Next(1,3);
					silaUtoku = nepratele[i].VypoctiUtok(typUtokuNepritele);
					switch (typUtokuNepritele) {
						case 1:
							Console.WriteLine(nepratele[i].Jmeno + " útočí zblízka!");
							obrana = postavaHrace.Obrana;
							break;
						case 2:
							Console.WriteLine(nepratele[i].Jmeno + " útočí na dálku!");
							obrana = postavaHrace.Uhybani;
							break;
					}
					
					if (silaUtoku > obrana) {
						Console.ForegroundColor = ConsoleColor.Red;
						Console.WriteLine("Protivník ti ubral " + (silaUtoku - obrana) + " životů");
						postavaHrace.Zivoty -= silaUtoku - obrana;
						if (postavaHrace.Zivoty <= 0) {
							Console.WriteLine(nepratele[i].Jmeno + " porazil " + postavaHrace.Jmeno);
							break;
						}
						Console.ResetColor();
					}
					else {
						Console.ForegroundColor = ConsoleColor.Blue;
						Console.WriteLine("Odrazil jsi útok");
						Console.ResetColor();
					}
				}
				
				if (postavaHrace.Zivoty > 0) {
					Console.ForegroundColor = ConsoleColor.Green;
					Console.WriteLine("Vyhrál jsi souboj! Počet turnajů, které tě dělí od vítězství: " + (nepratele.Length - i - 1));
					Console.ResetColor();
					Console.ReadKey(false);
				}
				else {
					Console.ForegroundColor = ConsoleColor.Red;				
					Console.WriteLine("Bohůžel jsi byl poražen, zkus to někdy příště.");
					Console.ResetColor();
					Console.ReadKey(false);
					break;
				}
				Console.Clear();
			}
			if (postavaHrace.Zivoty > 0) {
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine("Vyhrál jsi celý Turnaj. Gratulujeme!");
				Console.ResetColor();
				Console.ReadKey(false);
			}
		}
	}
}