﻿/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Honza
 * Datum: 24.7.2012
 * Čas: 19:16
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;
using System.Threading;

namespace SbiraniPredmetu
{
	enum StavHry {
		Probiha, Vyhra, Prohra, Ukonceni
	}
	
	class HerniSvet {
		
		private ConsoleKey posledniZmackutaSipka;
		
		private int pocetStrel = 3;
		private int[,] Mapa;
		const int MapaSirka = 24;
		const int MapaVyska = 24;
		
		const int Prekazka = 3;
		const int Predmet = 2;
		const int Vychod = 1;
		const int VolneMisto = 0;
		
		const int PocetPredmetu = 3;
		
		private Random rand = new Random();
		private int PocetPrekazek = 0;
		
		public StavHry StavHry = StavHry.Probiha;
		
		private int xSouradniceHrace = 1, ySouradniceHrace = 1;
		private int xPredchoziSouradniceHrace = 1, yPredchoziSouradniceHrace = 1;
		
		int ZbyvajiciPredmety;
		
		public int XSouradniceHrace {
			get {
				return xSouradniceHrace;
			}
			set {
				if (value >= 0 && value < MapaSirka - 1 + 1) {
					if (Mapa[value, ySouradniceHrace] == Prekazka) {
						return;
					}
					Mapa[xSouradniceHrace,ySouradniceHrace] = Prekazka;
					xSouradniceHrace = value;
				}
			}
		}
		
		public int YSouradniceHrace {
			get {
				return ySouradniceHrace;
			}
			set {
				if (value >= 0 && value < MapaVyska - 1 + 1) {
					if (Mapa[xSouradniceHrace, value] == Prekazka) {
						return;
					}
					Mapa[xSouradniceHrace,ySouradniceHrace] = Prekazka;
					ySouradniceHrace = value;
				}
			}
		}
		
		public HerniSvet() {
			Mapa = new int[MapaSirka + 50,MapaVyska + 50];
			PocetPrekazek = rand.Next(5, 10);
			
			NastavOhraniceni();
			NastavPrekazky();
			NastavPredmety();
			NastavVychod();
			
			for (int y = 0; y < MapaVyska; y++) { 
				for (int x = 0; x < MapaSirka; x++) {
					if (Mapa[x, y] == Predmet) {
						ZbyvajiciPredmety++;
					}
				}
			}
			
			ZobrazHerniSvet();
		}
		
		private void ZobrazHerniSvet() {
			for (int y = 0; y < MapaVyska; y++) { 
				for (int x = 0; x < MapaSirka; x++) {
					if (Mapa[x,y] == Prekazka) { 
						Console.SetCursorPosition(x,y);
						Console.Write("#");
					}
					else if (Mapa[x,y] == Predmet) { 
						Console.ForegroundColor = ConsoleColor.Green;
						Console.SetCursorPosition(x,y);
						Console.Write("*");
						Console.ResetColor();
					}
					else if (Mapa[x,y] == Vychod) { 
						Console.ForegroundColor = ConsoleColor.Yellow;
						Console.SetCursorPosition(x,y);
						Console.Write("V");
						Console.ResetColor();
					}
				}
			}
		}
		
		private void NastavOhraniceni() {
			for (int y = 0; y < MapaVyska; y++) {
				Mapa[0,y] = Prekazka;
				Mapa[MapaSirka-1, y] = Prekazka;
			}
			for (int x = 0;x < MapaSirka; x++) {
				Mapa[x,0] = Prekazka;
				Mapa[x,MapaVyska-1] = Prekazka;
			}
		}
		private void NastavPrekazky() {
			for (int i = 0; i < PocetPrekazek; i++) {
				
				int typPrekazky = rand.Next(1,4);
				int x = rand.Next(1, MapaSirka - 1 + 1 - 1);
				int y = rand.Next(1, MapaVyska - 1 + 1 - 1);
				int pocetPrekazek = rand.Next(10, 25);
				
				switch(typPrekazky) {
					case 1:
						for (int j = 0; j < pocetPrekazek;j++) {
							Mapa[x + j,y] = Prekazka;
						}
						break;
					case 2:
						for (int j = 0; j < pocetPrekazek;j++) {
							
							Mapa[x,y + j] = Prekazka;
						}		
						break;
					case 3:
						for (int j = 0; j < pocetPrekazek;j++) {
							Mapa[x + j,y + j] = Prekazka;
						}
						break;
				}
			}
		}
		private void NastavPredmety() {
			for (int i = 0; i < PocetPredmetu; i++) {
				int x = rand.Next(1, MapaSirka - 1 + 1 - 1);
				int y = rand.Next(1, MapaVyska - 1 + 1 - 1);
				if (Mapa[x,y] == Predmet) {
					i--;
				}
				else {
					Mapa[x,y] = Predmet;	
				}
			}
		}
		private void NastavVychod() {
			int x = 0;
			int y = 0;
			int vpravoVlevoNahoreDole = rand.Next(1,5);
			switch (vpravoVlevoNahoreDole) {
				case 1:
					x = MapaSirka - 1;
					y = rand.Next(1, MapaVyska - 1 + 1);
					break;
				case 2:
					x = 0;
					y = rand.Next(1, MapaVyska - 1 + 1);		
					break;
				case 3:
					x = rand.Next(1, MapaSirka - 1 + 1);
					y = 0;
					break;
				case 4:
					x = MapaVyska - 1;
					y = rand.Next(1, MapaSirka - 1 + 1);
					break;
			}
			Mapa[x,y] = Vychod;
		}
		
		public void ZpracovaniPohybu() {
			ConsoleKeyInfo stisknutaKlavesa = Console.ReadKey(true);
			switch (stisknutaKlavesa.Key) {
				case ConsoleKey.DownArrow:
					YSouradniceHrace++;
					posledniZmackutaSipka = ConsoleKey.DownArrow;
					break;
				case ConsoleKey.UpArrow:
					YSouradniceHrace--;
					posledniZmackutaSipka = ConsoleKey.UpArrow;
					break;
				case ConsoleKey.LeftArrow:
					XSouradniceHrace--;
					posledniZmackutaSipka = ConsoleKey.LeftArrow;
					break;
				case ConsoleKey.RightArrow:
					XSouradniceHrace++;
					posledniZmackutaSipka = ConsoleKey.RightArrow;
					break;
				case ConsoleKey.Escape:
					StavHry = StavHry.Ukonceni;
					break;
				case ConsoleKey.Spacebar:
					if (pocetStrel == 0)
						return;
					switch (posledniZmackutaSipka) {
						case ConsoleKey.DownArrow:
							Strilej("dolů");
							break;
						case ConsoleKey.UpArrow:
							Strilej("nahoru");
							break;
						case ConsoleKey.LeftArrow:
							Strilej("vlevo");
							break;
						case ConsoleKey.RightArrow:
							Strilej("vpravo");
							break;								
					}
					break;
			}
		}
		
		public void AktualizujZobrazeni() {
			Console.SetCursorPosition(xPredchoziSouradniceHrace,yPredchoziSouradniceHrace);
			Console.BackgroundColor = ConsoleColor.White;
			Console.Write(' ');
			
			Console.SetCursorPosition(XSouradniceHrace,YSouradniceHrace);
			Console.BackgroundColor = ConsoleColor.Red;
			Console.Write('X');
			Console.ResetColor();
			
			xPredchoziSouradniceHrace = XSouradniceHrace;
			yPredchoziSouradniceHrace = YSouradniceHrace;
			
			Console.SetCursorPosition(MapaSirka + 2,0);
			Console.WriteLine("Předmětů zbývá sebrat: " + ZbyvajiciPredmety);
		}
		
		protected void Strilej(string smer) {
			pocetStrel--;
			bool zasah = false;
			int x = XSouradniceHrace;
			int y = YSouradniceHrace;
			bool prvniStrela = true;
			while (!zasah) {
				if (!prvniStrela) {
					if (Mapa[x,y] != Predmet && Mapa[x,y] != Vychod) {
						Console.SetCursorPosition(x,y);
						Console.Write(' ');
					}
				}
				switch (smer) {
					case "dolů":
						if (Mapa[x, y + 1] == Prekazka) {
							if (x != XSouradniceHrace && y != YSouradniceHrace)	{
								if (Mapa[x,y] != Predmet && Mapa[x,y] != Vychod) {
									Console.SetCursorPosition(x,y);
									Console.Write(' ');
								}
							}
						 	Console.SetCursorPosition(x,y + 1);
						 	Console.Write(' ');
							Mapa[x,y + 1] = VolneMisto;
							return;
						}
						else {
							Console.SetCursorPosition(x,y + 1);
							y += 1;
						}						
						break;
					case "nahoru":
						 if (Mapa[x, y - 1] == Prekazka) {
							if (x != XSouradniceHrace && y != YSouradniceHrace) {
								if (Mapa[x,y] != Predmet && Mapa[x,y] != Vychod) {
									Console.SetCursorPosition(x,y);
									Console.Write(' ');
								}
							}
						 	Console.SetCursorPosition(x,y - 1);
						 	Console.Write(' ');
							Mapa[x,y - 1] = VolneMisto;
							return;
						}
						else {
							Console.SetCursorPosition(x,y - 1);
							y -= 1;
						}						
						break;
					case "vlevo":
						if (Mapa[x - 1, y] == Prekazka) {
							if (x != XSouradniceHrace && y != YSouradniceHrace) {
								if (Mapa[x,y] != Predmet && Mapa[x,y] != Vychod) {
									Console.SetCursorPosition(x,y);
									Console.Write(' ');
								}
							}
						 	Console.SetCursorPosition(x - 1,y);
						 	Console.Write(' ');
							Mapa[x - 1,y] = VolneMisto;
							return;
						}
						else {
							Console.SetCursorPosition(x - 1,y);
							x -= 1;
						}						
						break;
					case "vpravo":
						if (Mapa[x + 1, y] == Prekazka) {
							if (x != XSouradniceHrace && y != YSouradniceHrace) {
								if ((Mapa[x,y] != Predmet) && (Mapa[x,y] != Vychod)) {
									Console.SetCursorPosition(x,y);
									Console.Write(' ');
								}
							}
						 	Console.SetCursorPosition(x + 1,y);
						 	Console.Write(' ');
							Mapa[x + 1,y] = VolneMisto;
							return;
						}
						else {
							Console.SetCursorPosition(x + 1,y);
							x += 1;
						}						
						break;
				}
				Console.ForegroundColor = ConsoleColor.Blue;
				if (x >= (MapaSirka + 25) || x <= 0 || y >= (MapaVyska + 25) || y <= 0) {
					return;
				}
				if ((Mapa[x,y] != Predmet) && (Mapa[x,y] != Vychod)) {
						Console.Write('.');
				}
				Console.ResetColor();
				prvniStrela = false;
				Thread.Sleep(50);
			}
		}
		
		public void ZkontrolujPolicko() {
			if (Mapa[XSouradniceHrace,YSouradniceHrace] == Predmet) { 
				Mapa[XSouradniceHrace,YSouradniceHrace] = 0;
				ZbyvajiciPredmety--;
			}
			else if (Mapa[XSouradniceHrace,YSouradniceHrace] == Vychod && ZbyvajiciPredmety == 0) {
				StavHry = StavHry.Vyhra;
			}
			else if (Mapa[XSouradniceHrace,YSouradniceHrace] == Vychod && ZbyvajiciPredmety > 0) {
				StavHry = StavHry.Prohra;
			}
			if (Mapa[XSouradniceHrace,YSouradniceHrace] != Vychod) {
				if (Mapa[XSouradniceHrace + 1,YSouradniceHrace] == Prekazka &&
				    Mapa[XSouradniceHrace - 1,YSouradniceHrace] == Prekazka && 
				    Mapa[XSouradniceHrace,YSouradniceHrace - 1] == Prekazka && 
				    Mapa[XSouradniceHrace,YSouradniceHrace + 1] == Prekazka) {
					if (pocetStrel == 0) {
						StavHry = StavHry.Prohra;
					}
				}
			}
		}
	}
	
	static class HlavniNabidka {
		
		public static int ZobrazHlavniNabidku() {
			
			string[] polozkyNabidky = new string[3];
			polozkyNabidky[0] = "Nová hra";
			polozkyNabidky[1] = "Instrukce";
			polozkyNabidky[2] = "Konec";
			
			int zvolenaPolozka = 0;
			bool vyberDokoncen = false;
			
			Console.Clear();
			
			while (!vyberDokoncen) {
				Console.SetCursorPosition(0,3);
				for (int i = 0; i < polozkyNabidky.Length ;i++) {
					if (zvolenaPolozka == i) {
						Console.BackgroundColor = ConsoleColor.Blue;
					}
					Console.WriteLine(polozkyNabidky[i]);
					Console.ResetColor();
				}
				ConsoleKeyInfo StisknutaKlavesa = Console.ReadKey(true);
				if (StisknutaKlavesa.Key == ConsoleKey.DownArrow) {
					zvolenaPolozka++;
					if (zvolenaPolozka == polozkyNabidky.Length) {
						zvolenaPolozka = 0;
					}
				}
				else if (StisknutaKlavesa.Key == ConsoleKey.UpArrow) {
					zvolenaPolozka--;
					if (zvolenaPolozka < 0) {
						zvolenaPolozka = polozkyNabidky.Length - 1;
					}
				}
				else if (StisknutaKlavesa.Key == ConsoleKey.Enter) {
					vyberDokoncen = true;
				}
			}
			return zvolenaPolozka;
		}
	}
	
	class Program
	{
		const int NovaHra = 0;
		const int Instrukce = 1;
		const int Konec = 2;
		
		public static void Main(string[] args)
		{
			bool hraPokracuje = true;

			while (hraPokracuje) {
				int vysledekVolby = HlavniNabidka.ZobrazHlavniNabidku();
				switch (vysledekVolby) {
					case NovaHra:
						Console.Clear();
						HerniSvet herniSvet = new HerniSvet();
						herniSvet.AktualizujZobrazeni();
						while (herniSvet.StavHry == StavHry.Probiha) {
							herniSvet.ZpracovaniPohybu();
							herniSvet.ZkontrolujPolicko();
							herniSvet.AktualizujZobrazeni();
						}
						switch(herniSvet.StavHry) {
							case StavHry.Ukonceni:
								Console.Clear();
								Console.WriteLine("Hra ukončena uživatelem!");
								break;
							case StavHry.Vyhra:
								Console.Clear();
								Console.WriteLine("Vyhrál jsi!");
								break;
							case StavHry.Prohra:
								Console.Clear();
								Console.WriteLine("Prohrál jsi!");
								break;
						}
						Console.ReadKey(true);
						break;
					case Instrukce:
						Console.Clear();
						Console.Write("Instrukce\nOvládání:\n");
						Console.Write("\"šipky\"");
						Console.Write("\nCíl hry: Sesbírej všechny předměty a dojdi k východu");
						Console.ReadKey();
						break;
					case Konec:
						hraPokracuje = false;
						break;
				}
			}
			
		}
	}
}